

# change this to the folder where gatb-core is
GATB=$(shell pwd)/gatb-core/gatb-core/
GATB_LIB=$(GATB)/build/lib
OPTIM=-O3
GIT_SHA1=$(shell git log -1 --pretty=format:"%h %cs")
CXXFLAGS =  -Wall -Wno-unknown-pragmas -Wno-unused-function -std=c++14 $(OPTIM) -g  -I$(GATB)/src  -I$(GATB)/build/include -I$(GATB)/thirdparty -I $(shell pwd)/src/ -I $(shell pwd)/lib -DGIT_SHA1="\"$(GIT_SHA1)\""
LDFLAGS=   -L$(GATB_LIB) -lgatbcore  -lpthread -lhdf5 -lz -std=c++14 -g $(OPTIM) -ldl # -static
SRCS = $(wildcard src/Class/*/*.cpp) $(wildcard src/Tools/*.cpp) src/main.cpp
OBJS = $(patsubst %.cpp,%.o,$(SRCS))

EXE=filt3r

$(EXE): src/main.o $(OBJS)
	$(CXX) -o $@ $^  $(LDFLAGS)

$(EXE)-static: src/main.o $(OBJS)
	$(CXX) -o $@ $^  $(LDFLAGS) -static

%.o: %.cpp %.hpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(EXE).a: $(OBJS)
	ar rc $@ $^

tests:
	make -C tests tests

gatb: gatb-core/gatb-core/build/lib/libgatbcore.a

gatb-core/gatb-core/build/lib/libgatbcore.a:
	git clone https://github.com/GATB/gatb-core.git || (cd gatb-core && git pull)
	cd gatb-core/gatb-core && mkdir -p build && cd build && cmake .. && make

clean:
	rm -f $(EXE) $(EXE).a
	rm -f $(OBJS)

.PHONY: gatb clean tests
