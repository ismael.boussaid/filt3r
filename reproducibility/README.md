We provide a Snakefile to reproduce the experiments presented in the main paper of FiLT3r.

Before executing snakemake, you should fill the paths in `config.json`.

* `data_dir`: directory in which the `fastq.gz` files will be downloaded
* `results_dir`: directory in which the results will be stored
* `reference_genome`: path to a **FASTA file** containing GRCh37 (*eg.* download and uncompress [this file](http://ftp.ensembl.org/pub/grch37/current/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.dna.primary_assembly.fa.gz))
* `reference_sequence`: the reference sequence for the analysis (by default exons 14-15 of FLT3)
* `sra_accession`: file that contains the list of SRA files to retrieve and on which the software will be launched. In the current directory, `accessions.txt` contain the SRR accessions to the 185 AML patients and the `accessions-CCLE.tsv` contain the SRR accessions to the 152 RNA-Seq CCLE dataset used in our paper.

For reference, the expected results obtained with the gold-standard molecular biology method on the 185 AML patients is detailed in the `genescan_sra_corrected.csv` file.
