#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <list>
#include <vector>
#include <utility>

#include <Class/Cleaning/CleanedRead.hpp>
#include <Tools/filter_tools.hpp>
#include <cereal/archives/binary.hpp>
#include "../data.hpp"

TEST_CASE( "Testing removeAmbiguities 1", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //               0    1   2    3            6                      11                    16
    getFilteredRead({{}, {}, {1}, {2}, {}, {}, {5}, {6}, {7}, {}, {}, {1,5,10,20}, {2,6,11,21}, {3,7,12,22}, {4,8,13,23}, {}, {1}});

  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "", "");
  CleanedRead cleaned (read, filter.get(), 2);

  cleaned.removeAmbiguities();

  vector<int> cleaned_pos = cleaned.getCleanedPositions();

  vector<int> expected_clean =
    { -1, -1, 1, 2, -1, -1, 5, 6, 7, -1, -1, 10, 11, 12, 13, -1, 1};
  REQUIRE(cleaned_pos.size() == expected_clean.size());
  
  for (size_t i = 0; i < cleaned_pos.size(); i++) {
    INFO("Testing for i=" <<i);
    REQUIRE(cleaned_pos[i] == expected_clean[i]);
  }

  char *tmpname  = tmpnam(NULL);
  std::ofstream outfile(tmpname);
  cereal::BinaryOutputArchive boa(outfile);
  cleaned.save(boa);
  outfile.close();
  
  std::ifstream infile(tmpname);
  cereal::BinaryInputArchive bia(infile);
  CleanedRead read2;
  read2.load(bia);

  REQUIRE(read2.getCleanedPositions() == cleaned.getCleanedPositions());
  REQUIRE(read2.getFilteredRead()->getSequence() == cleaned.getFilteredRead()->getSequence());
  REQUIRE(read2.getFilteredRead()->getPositions() == cleaned.getFilteredRead()->getPositions());
  REQUIRE(read2.getFilteredRead()->isReversed() == cleaned.getFilteredRead()->isReversed());
}

TEST_CASE( "Testing removeAmbiguities 2", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //               0    1   2    3            6                      11                    16
    getFilteredRead({{}, {}, {1}, {0,2,10}, {}, {}, {5,50}, {6}, {7}, {}, {}, {1,5,10,20}, {}, {3,7,12,22}, {}, {}, {}});

  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "", "");
  CleanedRead cleaned (read, filter.get(), 2);

  cleaned.removeAmbiguities();

  vector<int> cleaned_pos = cleaned.getCleanedPositions();

  vector<int> expected_clean =
    { -1, -1, 1, 2, -1, -1, 5, 6, 7, -1, -1, 10, -1, 12, -1, -1, -1};
  REQUIRE(cleaned_pos.size() == expected_clean.size());
  
  for (size_t i = 0; i < cleaned_pos.size(); i++) {
    INFO("Testing for i=" <<i);
    REQUIRE(cleaned_pos[i] == expected_clean[i]);
  }
}

TEST_CASE( "Testing removeAmbiguities 3", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //               0    1   2    3            6                      11                    16
    getFilteredRead({{0,1,10}, {}, {}, {}, {1,5,50}, {}, {}, {}, {}, {1,5,10,20}, {}, {3,7,12,22}});

  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "", "");
  CleanedRead cleaned (read, filter.get(), 2);

  cleaned.removeAmbiguities();

  vector<int> cleaned_pos = cleaned.getCleanedPositions();

  vector<int> expected_clean =
    { 1, -1, -1, -1, 5, -1, -1, -1, -1, 10, -1, 12};
  REQUIRE(cleaned_pos.size() == expected_clean.size());
  
  for (size_t i = 0; i < cleaned_pos.size(); i++) {
    INFO("Testing for i=" <<i);
    REQUIRE(cleaned_pos[i] == expected_clean[i]);
  }

}

TEST_CASE( "Testing removeMutations 1", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //                0    1    2   3   4   5   6    7
    getFilteredRead({{1}, {2}, {3}, {}, {}, {}, {7}, {8}});

  read->getSequence() = "TGCAGAAT";
  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "sample1.fa", "");
  CleanedRead cleaned (read, filter.get(), 2);
  
  cleaned.removeAmbiguities();
  cleaned.removeMutations();

  std::vector<int> expected = {1, 2, 3, 4, 5, 6, 7, 8};

  REQUIRE( expected == cleaned.getCleanedPositions() );
  REQUIRE( "TGCAGTAT" == cleaned.getCorrectedRead() );
}

TEST_CASE( "Testing removeMutations 2", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //                0    1    2   3   4   5   6    7
    getFilteredRead({{1}, {2}, {}, {}, {}, {}, {7}, {8}, {}, {}});

  read->getSequence() = "TGCATGATCC"; // mutation on the T
  //                     ||||  ||||
  // ref:                TGCAGAATCCTG
  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "sample2.fa", "");
  CleanedRead cleaned (read, filter.get(), 2);
  
  cleaned.removeAmbiguities();
  cleaned.removeMutations();

  std::vector<int> expected = {1, 2, 3, 4, 5, 6, 7, 8, -1, -1};

  REQUIRE( expected == cleaned.getCleanedPositions() );
  REQUIRE( "TGCAGAATCC" == cleaned.getCorrectedRead() );

  //

  read =
    //                0    1    2    3  4   5   6    7
    getFilteredRead({{6}, {7}, {8}, {}, {}, {}, {2}, {3}, {}, {}});

  read->getSequence() = "AATCCCGCAG"; // insertion (C) + duplication (GCAG)
  //                     ||||| 
  // ref:          ATGCAGAATCCTG
  filter = getClassFilter(3, "sample2.fa", "");
  cleaned = CleanedRead(read, filter.get(), 2);
  
  cleaned.removeAmbiguities();
  cleaned.removeMutations();

  expected = {6, 7, 8, -1, -1, -1, 2, 3, -1, -1};

  REQUIRE( expected == cleaned.getCleanedPositions() );
  REQUIRE( "AATCCCGCAG" == cleaned.getCorrectedRead() );
  
}

TEST_CASE( "Testing remove spurious break 1", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //                0    1    2   3   4   5   6    7
    getFilteredRead({{1}, {2}, {3}, {}, {10}, {}, {7}, {8}});

  read->getSequence() = "TGCAGAAT";
  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "sample1.fa", "");
  CleanedRead cleaned (read, filter.get(), 2);

  cleaned.removeAmbiguities();
  cleaned.removeSpuriousGaps();

  std::vector<int> expected = {1, 2, 3, -1, -1, -1, 7, 8};

  REQUIRE( expected == cleaned.getCleanedPositions() );


  CleanedRead cleaned2 (read, filter.get(), 1);

  cleaned2.removeAmbiguities();
  cleaned2.removeSpuriousGaps();

  std::vector<int> expected2 = {1, 2, 3, -1, 10, -1, 7, 8};

  REQUIRE( expected2 == cleaned2.getCleanedPositions() );
}

TEST_CASE( "Testing remove spurious break + mutation", "[CleanedRead]" ) {
  std::shared_ptr<FilteredRead> read =
    //                0    1    2   3   4   5   6    7
    getFilteredRead({{1}, {2,5}, {}, {44}, {}, {}, {7,3}, {8}, {}, {}});

  read->getSequence() = "TGCATGATCC"; // mutation on the T
  //                     ||||  ||||
  // ref:                TGCAGAATCCTG
  std::shared_ptr<ClassFilter> filter = getClassFilter(3, "sample2.fa", "");
  CleanedRead cleaned (read, filter.get(), 2);
  
  cleaned.removeAmbiguities();
  cleaned.removeSpuriousGaps();
  cleaned.removeMutations();

  std::vector<int> expected = {1, 2, 3, 4, 5, 6, 7, 8, -1, -1};

  REQUIRE( expected == cleaned.getCleanedPositions() );
  REQUIRE( "TGCAGAATCC" == cleaned.getCorrectedRead() );
}
