#include <vector>
#include <string>
#include <Class/Filter/FilteredReads.hpp>
#include <Class/Filter/ClassFilter.hpp>

std::shared_ptr<FilteredRead> getFilteredRead(std::vector<std::vector<int> > pos, string seq = "") {
  if (seq == "")
    for (auto i: pos)
      seq += "A";
  std::shared_ptr<FilteredRead> r(new FilteredRead(seq, false));

  size_t i = 0;
  for (std::vector<int> &p: pos) {
    r->addMatchingKmers(i++, p);
  }

  return r;
}

std::shared_ptr<ClassFilter> getClassFilter(int kmer, std::string transcript,
                                            std::string sequences) {
  std::shared_ptr<ClassFilter> filter(new ClassFilter(kmer, transcript, sequences, 1));
  return filter;
}
