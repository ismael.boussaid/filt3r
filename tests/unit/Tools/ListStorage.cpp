#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <Tools/ListStorage.hpp>

TEST_CASE( "Testing MemoryListStorage", "" ) {
  MemoryListStorage<int> s;

  std::shared_ptr<int> i = std::make_shared<int>(3),
    j = std::make_shared<int>(5);

  REQUIRE(s.size() == 0);
  s.appendData(i);
  REQUIRE(s.size() == 1);
  s.appendData(j);
  REQUIRE(s.size() == 2);

  s.iterate();
  REQUIRE(s.hasData());
  REQUIRE(*(s.getCurrentData()) == 3);
  s.next();
  REQUIRE(s.hasData());
  REQUIRE(*(s.getCurrentData()) == 5);
  s.next();
  REQUIRE(! s.hasData());
}

TEST_CASE( "Testing DiskListStorage", "" ) {
  DiskListStorage<int> s;

  std::shared_ptr<int> i = std::make_shared<int>(3),
    j = std::make_shared<int>(5);
  

  REQUIRE(s.size() == 0);
  s.appendData(i);
  REQUIRE(s.size() == 1);
  s.appendData(j);
  REQUIRE(s.size() == 2);

  s.iterate();
  REQUIRE(s.hasData());
  REQUIRE(*(s.getCurrentData()) == 3);
  s.next();
  REQUIRE(s.hasData());
  REQUIRE(*(s.getCurrentData()) == 5);
  s.next();
  REQUIRE(! s.hasData());
}
