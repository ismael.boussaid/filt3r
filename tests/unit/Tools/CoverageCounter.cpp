#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <array>
#include <vector>
#include <utility>

#include <Tools/CoverageCounter.hpp>
#include "../data.hpp"

TEST_CASE( "Testing get", "[CoverageComputer]" ) {
  std::list<std::shared_ptr<FilteredRead> > reads;
  reads.push_back(getFilteredRead({{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}, {30}, {31}, {32}, {}, {}, {}, {}, {10}}, "TGGCgtCAACCTTCACCATC"));
  reads.push_back(getFilteredRead({{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}}, "TGGCACCAACCT"));
  reads.push_back(getFilteredRead({{1},{2},{3},{},{},{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}}, "ACCATTGGCACCAACCT"));

  ClassFilter *filter = getClassFilter(3, "", "").get();

  CoverageCounter counter(300);
  size_t i = 0;
  for (auto r: reads) {
    CleanedRead c(r, filter, 1);
    c.clean();
    counter.add(c, i / 2 + 1); // 1 for the two first reads and 2 for the last one
    i++;
  }

  REQUIRE(counter.get(0) == 0);
  REQUIRE(counter.get(198) == 4);
  REQUIRE(counter.get(50) == 4);
  REQUIRE(counter.get(30) == 1);
  REQUIRE(counter.get(1) == 2);
  REQUIRE(counter.get(201) == 4);

  REQUIRE(counter.get(0, 1) == 'N');
  REQUIRE(counter.get(1, 1) == 'A');
  REQUIRE(counter.get(1, 0) == 'A');
  REQUIRE(counter.get(198, .5) == 'G');
  REQUIRE(counter.get(199, .5) == 'N');
  REQUIRE(counter.get(200, 1) == 'C');
  REQUIRE(counter.get(200, 0) == 'C');

  REQUIRE(counter.nuc_counts(201) == std::array<size_t, 4>({3, 0, 1, 0}));
  REQUIRE(counter.get(201, 1) == 'A');
  REQUIRE(counter.get(201, .1) == 'A');
  REQUIRE(counter.get(201, .15) == 'G');
  REQUIRE(counter.get(201, .25) == 'G');
  REQUIRE(counter.get(201, .4) == 'G');
  REQUIRE(counter.get(201, .6) == 'A');
  REQUIRE(counter.get(202, .25) == 'T');
  REQUIRE(counter.get(202, .1, 3) == 'T');
  REQUIRE(counter.get(202, .1) == 'C');
  REQUIRE(counter.get(202, 1) == 'C');
}
