#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <list>
#include <vector>
#include <utility>

#include <Tools/KmerPosAnalyser.hpp>
#include <Tools/filter_tools.hpp>


TEST_CASE( "Testing getGaps()", "[KmerPosAnalyser]" ) {
  std::vector<std::vector<std::vector<int> > > positions =
    {
      // 0  1                     5        7
      { {}, {}, {1}, {1, 2}, {1}, {}, {2}, {}, {}, {3}},
      { {}, {}, {1}, {1, 2}, {1}, {}, {2}, {}, {}, {}},
      { {4}, {}, {1}, {1, 2}, {1}, {}, {2}, {}, {}, {3}}
    };

  std::vector<std::vector<std::pair<size_t, size_t> > > solutions = {
    { {0,1}, {5,5}, {7,8}},
    { {0,1}, {5,5}, {7, 9}},
    { {1,1}, {5, 5}, {7, 8}}
  };

  for (size_t i = 0; i < positions.size(); i++) {
    KmerPosAnalyser kpa(positions[i]);
    REQUIRE( kpa.getGaps() == solutions[i] );
  }
}


TEST_CASE( "Testing getDistance()", "[KmerPosAnalyser]" ) {
  std::vector<std::vector<int> >  positions =
    // 0     1      2    3    4     5      6        7
    { {5}, {6,10}, {7}, {8}, {9}, {1,10}, {2,11}, {3,12}};

  std::vector<std::tuple<int, size_t, size_t, size_t, float> > param_solutions = {
    {10, 0, 4, 1, 16./5},           // (4+0+4+4+4)/5 = 16/5
    {6, 0, 4, 1, 0},
    {6, 1, 5, 0, 1},
    {10, 0, 4, 5, 0},
    {10, 0, 4, 0, 21./5},         // 5*4 + 1
    {10, 0, 7, 5, 0}
  };

  KmerPosAnalyser kpa(positions);
  for (auto test: param_solutions) {
    int value = std::get<0>(test);
    size_t start_pos = std::get<1>(test);
    size_t end_pos = std::get<2>(test);
    size_t value_pos = std::get<3>(test);
    INFO("Launching getDistance(" << value << ", "
         << start_pos << ", "
         << end_pos << ", "
         << value_pos << ")");
    REQUIRE( kpa.getDistance(value, start_pos, end_pos,
                             value_pos)
             == Approx(std::get<4>(test)));
  }
}

TEST_CASE( "Testing getNonAmbiguousDistance()", "[KmerPosAnalyser]" ) {
  std::vector<int>  positions =
    //0  1   2  3   4   5
    { 5, 6, -1, -1, 9, 10};

  std::vector<std::tuple<int, size_t, size_t, size_t, float> > param_solutions = {
    {10, 0, 4, 1, 3.*4/3},           
    {6, 0, 4, 1, 0},
    {5, 1, 5, 0, 0},
    {0, 1, 5, 0, 5}
  };

  for (auto test: param_solutions) {
    int value = std::get<0>(test);
    size_t start_pos = std::get<1>(test);
    size_t end_pos = std::get<2>(test);
    size_t value_pos = std::get<3>(test);
    INFO("Launching getNonAmbiguousDistance(" << value << ", "
         << start_pos << ", "
         << end_pos << ", "
         << value_pos << ")");
    REQUIRE( KmerPosAnalyser::getNonAmbiguousDistance(positions, value, start_pos, end_pos,
                             value_pos)
             == Approx(std::get<4>(test)));
  }
}
