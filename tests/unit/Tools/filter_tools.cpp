#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <tuple>

#include <Tools/filter_tools.hpp>


TEST_CASE( "Testing name_to_pos()", "[filter_tools]" ) {
  REQUIRE( name_to_pos("X:12-25:1") == std::make_tuple("X", 12, 25, true) );
  REQUIRE( name_to_pos("XYZ:12-25431:-1") == std::make_tuple("XYZ", 12, 25431, false) );
  REQUIRE( name_to_pos("XYZ:212-25:-1") == std::make_tuple("", 0, 0, true) );
  REQUIRE( name_to_pos("XYZ212-25:-1") == std::make_tuple("", 0, 0, true) );
}

