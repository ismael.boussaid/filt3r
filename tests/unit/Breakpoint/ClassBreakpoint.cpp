#define CATCH_CONFIG_MAIN
#include <vector>

#include <catch.hpp>
#include <Class/Breakpoint/ClassBreakpoint.hpp>
#include <Class/Cleaning/CleanedRead.hpp>
#include <Tools/ListStorage.hpp>

#include "../data.hpp"

TEST_CASE( "Testing ClassBreakpoint", "[ClassBreakpoint]" ) {
  std::list<std::shared_ptr<FilteredRead> > reads;
  reads.push_back(getFilteredRead({{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}, {30}, {31}, {32}, {}, {}, {}, {}, {10}}));
  reads.push_back(getFilteredRead({{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}}));
  reads.push_back(getFilteredRead({{1},{2},{3},{},{},{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}}));
  ListStorage<CleanedRead> *cleaned = new MemoryListStorage<CleanedRead>();

  std::shared_ptr<ClassFilter> shared_filter = getClassFilter(10, "", "");
  ClassFilter *filter = shared_filter.get();

  for (auto r: reads) {
    std::shared_ptr<CleanedRead> cread = std::make_shared<CleanedRead>(r, filter, 2);
    cread->clean();
    cleaned->appendData(cread);
  }

  std::string ref(250, 'A');
  ClassBreakpoint cbp(cleaned, ref);
  cbp.searchBreakpoints(1, 1, filter->getReferencePos());
  REQUIRE(cbp.count() == 4);

  std::map<std::string, Breakpoint*> results = cbp.getResults();
  std::set<std::string> keys;
  for(auto const& m: results)
    keys.insert(m.first);
  REQUIRE(keys == std::set<std::string>({"200-50-153", "52-30-26", "32-10-27", "3-198--190"}));
  REQUIRE(results["200-50-153"]->duplicationSize() == 153);
  REQUIRE(results["200-50-153"]->count() == 3);
  REQUIRE(results["200-50-153"]->correctedCount() == 7);
  REQUIRE(results["200-50-153"]->getTotalOccurrence() == 7); // because we take correctedCount + min when total is too low
  REQUIRE(results["52-30-26"]->count() == 1);
  REQUIRE(results["52-30-26"]->getTotalOccurrence() == 2);
  REQUIRE(results["32-10-27"]->count() == 1);
  REQUIRE(results["3-198--190"]->count() == 1);
    
  ClassBreakpoint cbp2(cleaned, ref);
  cbp2.searchBreakpoints(3, 1, filter->getReferencePos());
  REQUIRE(cbp2.count() == 1);

  ClassBreakpoint cbp3(cleaned, ref);
  cbp3.searchBreakpoints(1, 2, filter->getReferencePos());
  REQUIRE(cbp3.count() == 1);

}

TEST_CASE( "Testing ClassBreakpoint 2", "[ClassBreakpoint]" ) {
  std::list<std::shared_ptr<FilteredRead> > reads;
//102, 103, 104, 105, null, null, null, null, null, null, null, 95, 96, 97, 98
  reads.push_back(getFilteredRead({{102}, {103}, {104}, {105}, {}, {}, {}, {}, {}, {}, {}, {95}, {96}, {97}, {98}, {}, {}, {}, {}}));
  reads.push_back(getFilteredRead({{101}, {102}, {103}, {104}, {105}, {}, {}, {}, {}, {}, {}, {}, {95}, {96}, {97}, {98}, {}, {}, {}, {}}));
  ListStorage<CleanedRead> *cleaned = new MemoryListStorage<CleanedRead>();

  ClassFilter *filter = getClassFilter(10, "", "").get();

  for (auto r: reads) {
    std::shared_ptr<CleanedRead> cread = std::make_shared<CleanedRead>(r, filter, 2);
    cread->clean();
    cleaned->appendData(cread);
  }

  std::string ref(150, 'A');
  ClassBreakpoint cbp(cleaned, ref);
  cbp.searchBreakpoints(1, 1, filter->getReferencePos());
  REQUIRE(cbp.count() == 1);

  std::map<std::string, Breakpoint*> results = cbp.getResults();
  std::set<std::string> keys;
  for(auto const& m: results)
    keys.insert(m.first);
  REQUIRE(keys == std::set<std::string>({"105-95-18"}));
  REQUIRE(results["105-95-18"]->count() == 2);
}
