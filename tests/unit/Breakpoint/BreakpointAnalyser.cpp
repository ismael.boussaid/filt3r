#define CATCH_CONFIG_MAIN
#include <vector>

#include <catch.hpp>
#include <Class/Breakpoint/BreakpointAnalyser.hpp>
#include <Class/Cleaning/CleanedRead.hpp>

#include "../data.hpp"

TEST_CASE( "Testing findStartPos", "[BreakpointAnalyser]" ) {
  std::shared_ptr<FilteredRead> read =
    //               0    1   2    3            6                      11                    16
    getFilteredRead({{}, {}, {1}, {2}, {}, {}, {1}, {2}, {3}, {}, {}, {1}, {2}, {3}, {}, {}, {1}});

  std::shared_ptr<CleanedRead> cleaned = make_shared<CleanedRead>(read, getClassFilter(10, "", "").get(), 2);
  cleaned->clean();
  
  BreakpointAnalyser bpa(cleaned);

  REQUIRE (bpa.findStartStop(1) == std::list<std::pair<size_t, size_t> >({{3,6} , {8,11}, {13,16}}));
  REQUIRE (bpa.findStartStop(2) == std::list<std::pair<size_t, size_t> >({{3,6}, {8,11}}));
  REQUIRE (bpa.findStartStop(3) == std::list<std::pair<size_t, size_t> >({{8,11}}));
  REQUIRE (bpa.findStartStop(4) == std::list<std::pair<size_t, size_t> >({}));
}

TEST_CASE( "Testing findBreakpoints", "[BreakpointAnalyser]" ) {
  std::shared_ptr<FilteredRead> read =
    getFilteredRead({{}, {}, {198}, {200}, {}, {}, {50}, {51}, {52}, {}, {}, {}, {30}, {31}, {32}, {}, {}, {}, {}, {10}});

  std::shared_ptr<CleanedRead> cleaned = make_shared<CleanedRead>(read, getClassFilter(10, "", "").get(), 2);
  cleaned->clean();
  BreakpointAnalyser bpa(cleaned);

  std::list<Breakpoint*> lbp = bpa.findBreakpoints(1);
  std::vector<Breakpoint*> bp = {std::begin(lbp), std::end(lbp) };

  REQUIRE(bp.size() == 3);
  
  REQUIRE(bp[0]->getStopPos() == 200);
  REQUIRE(bp[0]->getStartPos() == 50);
  REQUIRE(bp[0]->getGapSize() == 2);
  REQUIRE(bp[0]->count() == 1);
  REQUIRE(bp[0]->duplicationSize() == 153);
  REQUIRE(bp[0]->isDuplication());

  REQUIRE(bp[1]->getStopPos() == 52);
  REQUIRE(bp[1]->getStartPos() == 30);
  REQUIRE(bp[1]->getGapSize() == 3);
  REQUIRE(bp[1]->count() == 1);
  REQUIRE(bp[1]->duplicationSize() == 26);
  REQUIRE(bp[1]->isDuplication());

  REQUIRE(bp[2]->getStopPos() == 32);
  REQUIRE(bp[2]->getStartPos() == 10);
  REQUIRE(bp[2]->getGapSize() == 4);
  REQUIRE(bp[2]->count() == 1);
  REQUIRE(bp[2]->duplicationSize() == 27);
  REQUIRE(bp[2]->isDuplication());
  
  lbp = bpa.findBreakpoints(2);
  bp = {std::begin(lbp), std::end(lbp) };

  REQUIRE(bp.size() == 1);

  REQUIRE(bp[0]->getStopPos() == 52);
  REQUIRE(bp[0]->getStartPos() == 30);
  REQUIRE(bp[0]->getGapSize() == 3);
  REQUIRE(bp[0]->count() == 1);
  REQUIRE(bp[0]->duplicationSize() == 26);
  REQUIRE(bp[0]->isDuplication());
}
