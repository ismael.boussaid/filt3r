#!/usr/bin/perl -w
use strict;

use constant DEFAULT_COLUMN => 0;
use constant DEFAULT_SEP => '\s+';

use Getopt::Long qw(:config no_ignore_case bundling);
use Pod::Usage;

=head1 NAME

sequence_distribution – analyse the distribution of a numeric sequence

=head1 SYNOPSIS

sequence_distribution [options] [file]

Options:
    -h|--help
    --manual

    -c|--column <col>
    -f|--field <sep>
    
    -d|--decile
    -r|--quartile
    -i|--quintile
    -m|--median
    -p|--percentile
    -v|--vingintile
    -Q|--quantile <nb>

    -a|--all

    -l|--lower <nb>
    -g|--greater <nb>

    --correlation <col>
=cut

=head1 OPTIONS

=over 8

=item B<-c|--column>

Column identifier (starts at 0) that must be taken into account
for the analysis of the numeric sequence.
By default takes the first one (identifier 0).

=item B<-f|--field>

The field separator between columns.
Default is whitespaces.

=item B<-d|--decile>

Output first and last decile values

=item B<-r|--quartile>

Output first and last quartile values

=item B<-i|--quintile>

Output first and last quintile values

=item B<-m|--median>

Output median value

=item B<-p|--percentile>

Output first and last percentile values

=item B<-v|--vingintile>

Output first and last vingintile values

=item B<-Q|--quantile>

Output first and last <nb>-quantile values

=item B<-a|--all>

Output the values of all the quantiles for each quantile.

=item B<-l|--lower>

Output the percentage of values that are lower or equal to this value

=item B<-g|--greater>

Output the percentage of values that are greater or equal to this value.

=item B<--correlation>

Compute the correlation, the second set of values being found in column
<col>.

=back

=cut

my ($help, $manual, $field, $file, $median,$all, $correl);
my @column;
my @quantile;
my @lower;
my @greater;
$field = DEFAULT_SEP;

GetOptions("h|help", \$help,
           "manual", \$manual,
           "c|column=i", \@column,
           "f|field=s", \$field,
           "d|decile", sub{push @quantile, 10},
           "r|quartile", sub{push @quantile, 4},
           "i|quintile", sub{push @quantile, 5},
           "p|percentile", sub{push @quantile, 100},
           "m|median",\$median,
           "v|vingintile", sub{push @quantile, 20},
           "Q|quantile=i", \@quantile,
           "a|all", \$all,
           "correlation=i", \$correl,
           "l|lower=f", sub{push @lower, $_[1]},
           "g|greater=f", sub {push @greater, $_[1]});
pod2usage(1) if $help;
pod2usage(-verbose => 2) if $manual;

if (! @column) {
    push @column, DEFAULT_COLUMN;
}
if (@ARGV) {
    open($file, $ARGV[0]) or die("Unable to open file $ARGV[0]: $!\n");
} else {
    $file = \*STDIN;
}

if (defined $correl) {
    push @column, $correl;
}

my @current_cols;
my @numbers;
my @numbers_y;
my $ignore;

while (<$file>) {
    @current_cols = split /$field/;
    foreach my $index (@column) {
        if (defined $current_cols[$index]) {
            chomp $current_cols[$index];
            $ignore = 0;
            if ($current_cols[$index] !~ /^[0-9]*(\.[0-9]+)?$/) {
                if ($current_cols[$index] =~ /^[0-9]*(,[0-9]+)?$/) {
                    $current_cols[$index] =~ s/,/./;
                } else {
                    $ignore = 1;
                }
            }
            if (! $ignore) {
                if (defined $correl && $correl == $index) {
                    push @numbers_y, $current_cols[$index];
                } else {
                    push @numbers, $current_cols[$index];
                }
            }
        }
    }
}
close($file);

if (defined $correl && $#numbers != $#numbers_y) {
    print STDERR "The number of values in the two columns is not the same. Abort!\n";
}

my %nb_lower;
my %nb_greater;

my ($min, $max, $average, $average_y) = ($numbers[0], $numbers[0], $numbers[0]);
$average_y = $numbers_y[0] if (defined $correl);
my $total = $#numbers+1;
# Compute min, max, average
for (my $i = 1; $i <= $#numbers; $i++) {
    if ($min > $numbers[$i]) {
        $min = $numbers[$i];
    } elsif ($max < $numbers[$i]) {
        $max = $numbers[$i];
    }
    
    $average += $numbers[$i];
    $average_y += $numbers_y[$i]
        if (defined $correl);
}

if ($total > 0) {
    $average /= 1.*$total;
    $average_y /= 1.*$total
        if (defined $correl);
    # Compute standard deviation
    my $sdeviation = 0;
    my $sdeviation_y = 0;
    for (my $i = 0; $i <= $#numbers; $i++) {
        $sdeviation += ($average - $numbers[$i]) ** 2;
        $sdeviation_y += ($average_y - $numbers_y[$i]) ** 2
            if (defined $correl);

        # Compute lower and greater
        foreach my $low (@lower) {
            if ($numbers[$i] <= $low) {
                $nb_lower{$low}++;
            }
        }
        foreach my $great (@greater) {
            if ($numbers[$i] >= $great) {
                $nb_greater{$great}++;
            }
        }
    
    }
    #  Compute the sample variance
    $sdeviation = ($sdeviation * 1./($total-1)) **0.5; # emacs indentation bug -> /);
    $sdeviation_y = ($sdeviation_y * 1. / ($total-1)) **0.5; # emacs indentation bug -> /);
    
    printf "Count\t%d\nMin\t%s\nMax\t%s\nAverage\t%.3f\nStddev\t%.3f\n\n",
    $total, $min, $max, $average, $sdeviation;

    if (defined $correl) {
        my $r_coeff = 0;
        for (my $i=0; $i <= $#numbers; $i++) {
            $r_coeff += ($numbers[$i] - $average) * ($numbers_y[$i] - $average_y);
        }
        $r_coeff /= 1. * ($total - 1) * $sdeviation * $sdeviation_y;

        printf "Average_y\t%.3f\nSigma_y\t%.3f\nr\t%.3f\nr²\t%.3f\n\n",
        $average_y, $sdeviation_y, $r_coeff, $r_coeff * $r_coeff;
    }
    

# Sort numbers
    if (defined $median || @quantile) {
        @numbers_y = sort {$a <=> $b} @numbers_y
            if (defined $correl);
        @numbers = sort {$a <=> $b} @numbers;
    }

    if (defined $median) {
        printf "Median\t%s\n",$numbers[$total/2];
    }
    foreach my $quantile (@quantile) {
        if ($quantile < $total) {
            printf "%d-quantile\t", $quantile;
            if ($all) {
                # Output all quantiles
                for (my $j = 1; $j < $quantile; $j++) {
                    printf "%s\t", $numbers[$j * $total/$quantile];
                }
                printf "\n";
            } else {
                # Just output first and last values
                printf "%s\t%s\n", $numbers[$total/$quantile],
            $numbers[($quantile - 1) * $total / $quantile + 1]
            }
        }
    }
    
    foreach my $nb (@lower) {
        printf "%s-lower\t%d\t%.4f\n", $nb, $nb_lower{$nb}, 1.*$nb_lower{$nb}/$total;
    }
    foreach my $nb (@greater) {
        printf "%s-greater\t%d\t%.4f\n", $nb, $nb_greater{$nb}, 1.*$nb_greater{$nb}/$total;
    }
}



