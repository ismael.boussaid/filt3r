import os
import sys
from collections import defaultdict

def load(f, d, i):
    for line in open(f).readlines():
        fields = line.rstrip().split('\t')
        n = int(fields[1])
        v = float(fields[2])
        if i == 0 or sum([d[fields[0]][n][k] != None for k in range(0,i)]) > 0 or v>=.01:
            d[fields[0]][n][i] = v

nb = len(sys.argv)-1
d = defaultdict(lambda: defaultdict(lambda: [None for i in range(nb)]))
for i in range(1,len(sys.argv)):
    load(sys.argv[i], d, i-1)
for s in sorted(d.keys()):
    for n in sorted(d[s]):
        if not all([i==None or i < .01 for i in d[s][n]]):
            print(s,n,'\t'.join([str(i) if i is not None else '' for i in d[s][n]]), sep='\t')

