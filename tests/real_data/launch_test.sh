#!/bin/bash
set -e

PROCESSES=1
QUANTIF_CHANGE=10

git_date_of_ref() {
    git show -s --format=%ci "$1" | awk '{printf "%s_%s", $1,$2}'
}

compile() {
    BRANCH="$1"
    cd ../..
    git checkout "$BRANCH"
    make clean && make -j $PROCESSES
    git checkout -
    cd -
}


launch() {
    NAME="$1"
    CONFIG="$2"
    sed -ri 's@(results_dir.*)",@\1/'$NAME'",@' "$CONFIG"
    snakemake -j $PROCESSES --configfile "$CONFIG"
}

# gives in an array, the min, q1, the median, q4 and the max for the given field
get_bench_values() {
    local dir="$1"
    field="$2"
    local -n array=$3

    array=(`awk '{print $'$field'}' "$dir"/benchs/*.log | ./sequence_distribution.pl -m -Q 5 | awk '$1=="Min"{min=$2} $1=="Max"{max=$2} $1=="Median"{median=$2} $1=="5-quantile"{q1=$2; q5=$3} END {print min,q1,median,q5,max}'`)
}

display_results() {
    local -n old=$1
    local -n current=$2
    local old_branch="$3"
    local new_branch="$4"
    echo "$5"

    echo -e "     \tmin\tQ1\tmedian\tQ4\tmax"
    echo -e "$old_branch\t${old[0]}\t${old[1]}\t${old[2]}\t${old[3]}\t${old[4]}"
    echo -e "$new_branch\t${current[0]}\t${current[1]}\t${current[2]}\t${current[3]}\t${current[4]}"
    echo -en "delta"
    for i in $(seq 0 4); do
	echo -en "\t"$(awk -v a=${old[$i]} -v b=${current[$i]} 'BEGIN{printf "%.0f%%\n", (b-a)*100/a}')
    done
    echo
}

assess_bench() {
    local dir="$1"
    local old_branch="$2"
    local new_branch="$3"
    local dir_old_branch="$4"
    local dir_new_branch="$5"
    local old_results
    local current_results

    get_bench_values "$dir/$dir_new_branch" 1 current_results
    get_bench_values "$dir/$dir_old_branch" 1 old_results

    display_results old_results current_results "$old_branch" "$new_branch" "Time consumption"

    get_bench_values "$dir/$dir_new_branch" 4 current_results
    get_bench_values "$dir/$dir_old_branch" 4 old_results
    display_results old_results current_results "$old_branch" "$new_branch" "Memory consumption"
}

compare_results() {
    local dir="$1"
    local old_branch="$2"
    local new_branch="$3"

    changes=0

    python3 merge_results.py genescan_sra_corrected.csv "$dir/$old_branch/filt3r.k-12.tsv" "$dir/$new_branch/filt3r.k-12.tsv" > "$dir/$new_branch/compare_${old_branch}_${new_branch}.tsv"
    new_dup=$(cat "$dir/$new_branch/compare_${old_branch}_${new_branch}.tsv" | awk -F '\t' 'BEGIN{OFS="\t"} length($4)==0 && length($5)>0{print $0}')

    if [ ! -z "$new_dup" ]; then
	echo "*** NEW EVENTS FOUND ***"
	echo "$new_dup";
	changes=$((changes+1))
    fi

    lost_dup=$(cat "$dir/$new_branch/compare_${old_branch}_${new_branch}.tsv" | awk -F '\t' 'BEGIN{OFS="\t"} length($4)>0 && length($5)==0{print $0}')

    if [ ! -z "$lost_dup" ]; then
	echo "*** EVENTS LOST ***"
	echo "$lost_dup";
	changes=$((changes+1))
    fi

    quantif_changes=$(cat "$dir/$new_branch/compare_${old_branch}_${new_branch}.tsv" | awk -v q=$QUANTIF_CHANGE -F '\t' 'BEGIN{OFS="\t"} length($5)>0 && length($6)==0{var=($5-$4)*100/$4; if (var < -q || var > q) {printf "%s\t%.0f%%\n", $0, var}}')
    if [ ! -z "$quantif_changes" ]; then
	echo "*** SIGNIFICANT QUANTIFICATION CHANGES > ±$QUANTIF_CHANGE% ***"
	echo "$quantif_changes"
	changes=$((changes+1))
    fi

    if [ $changes -eq 0 ]; then
	echo "No significant change in detections/quantifications between both versions"
    else
	echo "Please review the changes between both versions"
    fi
}

if [ $# -lt 4 ]; then
    echo "$0 <processes> <old commit/branch> <new commit/branch> <dest dir> [data_dir]

processes: number of processors to use

if specified, the data_dir will be used to store the files or will use the files if they already exist.

The script will launch filt3r on the two commits and will then compare time and memory consumptions
and will check whether the results are similar between both versions
" >&2
    exit 1
fi

PROCESSES=$1
OLD_BRANCH="$2"
NEW_BRANCH="$3"
RESULTS_DIR="$4"
DATE_OLD_BRANCH=$(git_date_of_ref "$OLD_BRANCH")
DATE_NEW_BRANCH=$(git_date_of_ref "$NEW_BRANCH")
DIR_OLD_BRANCH=${OLD_BRANCH}-$DATE_OLD_BRANCH
DIR_NEW_BRANCH=${NEW_BRANCH}-$DATE_NEW_BRANCH
KEEP_DATA=0
if [ $# -ge 5 ]; then
    DATA_DIR="$5"
    KEEP_DATA=1
else
    DATA_DIR=$(mktemp -d)
fi
new_config=$(mktemp)
save_config=$(mktemp)
cp config.json $new_config
sed -ri 's@(data_dir.*)",@\1'$DATA_DIR'",@' $new_config
sed -ri 's@(results_dir.*)",@\1'$RESULTS_DIR'",@' $new_config
cp $new_config $save_config

CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

compile "$OLD_BRANCH"
launch "$DIR_OLD_BRANCH" $new_config
cp  $save_config $new_config
compile "$NEW_BRANCH"
launch "$DIR_NEW_BRANCH" $new_config

assess_bench "$RESULTS_DIR" "$OLD_BRANCH" "$NEW_BRANCH" "$DIR_OLD_BRANCH" "$DIR_NEW_BRANCH"

compare_results "$RESULTS_DIR" "$DIR_OLD_BRANCH" "$DIR_NEW_BRANCH"
