#include <algorithm>
#include <cmath>
#include <limits>

#include "KmerPosAnalyser.hpp"
#include <Tools/filter_tools.hpp>

KmerPosAnalyser::KmerPosAnalyser(std::vector<std::vector<int> > &positions):positions(positions){}

std::vector<std::pair<size_t, size_t> > KmerPosAnalyser::getGaps() {
  std::vector<std::pair<size_t, size_t> > results;
  size_t start_pos;
  bool is_in_N = false;

  size_t i = 0;
  for (auto &pos: positions) {
    if (pos.size() == 0) {
      if (! is_in_N) {
        is_in_N = true;
        start_pos = i;
      }
    } else {
      if (is_in_N) {
        is_in_N = false;
        results.push_back(std::make_pair(start_pos, i-1));
      }
    }
    i++;
  }
  if (is_in_N)
    results.push_back(std::make_pair(start_pos, i-1));
  return results;
}


float KmerPosAnalyser::getDistance(int value, size_t start_pos, size_t end_pos,
                                   size_t value_pos){
  float distance = 0;
  int delta = (start_pos - value_pos);
  size_t pos_considered = 0;
  for (size_t pos = start_pos; pos <= end_pos; pos++) {
    if (positions[pos].size() > 0) {
      int min = std::abs(positions[pos][0] - (value + delta));
      for (int p: positions[pos]) {
        int val = std::abs(p - (value + delta));
        if (min > val)
          min = val;
      }
      distance += min;
      pos_considered++;
    }
    delta++;                    // when moving to the next position, the value should increase by one
  }
  if (pos_considered == 0)
    return std::numeric_limits<float>::max();
  return distance / pos_considered;
}

float KmerPosAnalyser::getNonAmbiguousDistance(std::vector<int> &values, int value,
                                               size_t start_pos, size_t end_pos,
                                               size_t value_pos){
  float distance = 0;
  int delta = (start_pos - value_pos);
  size_t pos_considered = 0;
  for (size_t pos = start_pos; pos <= end_pos; pos++) {
    if (values[pos] > -1) {
      distance += std::abs(values[pos] - (value + delta));
      pos_considered++;
    }
    delta++;                    // when moving to the next position, the value should increase by one
  }
  if (pos_considered == 0)
    return std::numeric_limits<float>::max();
  return distance / pos_considered;
}

int KmerPosAnalyser::getPreviousGap(size_t pos) {
  while (pos != 0) {
    if (positions[pos].size() == 0)
      return pos;
    pos--;
  }
  return (positions[0].size() == 0) ? 0 : -1;
}

int KmerPosAnalyser::getNextGap(size_t pos) {
  while (pos < positions.size()) {
    if (positions[pos].size() == 0)
      return pos;
    pos++;
  }
  return -1;
}
