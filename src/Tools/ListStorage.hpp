#ifndef LIST_STORAGE_HPP
#define LIST_STORAGE_HPP

#include <list>
#include <cstdio>
#include <fstream>
#include <cereal/archives/binary.hpp>
#include <Class/Filter/FilteredReads.hpp>

/**
 * Storage of elements of type shared_ptr<T> in a queue-like structure.
 * The operations of writing and reading must be completely and independent
 * and cannot happened in an intertwined fashion.
 */
template <class T>
class ListStorage {
public:
  ListStorage(){}
  virtual ~ListStorage(){};

  virtual void appendData(std::shared_ptr<T> data) = 0;

  virtual size_t size() const = 0;
  
  /**
   * True if data exists at the current position
   */
  virtual bool hasData() const = 0;
  
  /**
   * Initialize the iteration over the data stored.
   */
  virtual void iterate() = 0;

  virtual void next() = 0;

  /**
   * @pre hasData()
   */ 
  virtual std::shared_ptr<T> &getCurrentData() = 0;

};


template <class T>
class MemoryListStorage: public ListStorage<T> {
public:
  MemoryListStorage() {}
  ~MemoryListStorage() {}

  void appendData(std::shared_ptr<T> data) override{
    list.push_back(data);
  }

  size_t size() const override{
    return list.size();
  }

  bool hasData() const override{
    return it != list.end();
  }
  void iterate() override{
    it = list.begin();
  }
  void next() override{
    it++;
  }
  std::shared_ptr<T> &getCurrentData() override{
    return *it;
  }
private:
  std::list<std::shared_ptr<T> > list;
  typename std::list<std::shared_ptr<T> >::iterator it;
};

template <class T>
class DiskListStorage: public ListStorage<T> {
public:
  DiskListStorage():nb(0),read_storage(std::tmpnam(nullptr)) {
    output = std::ofstream(read_storage, std::ios::binary);
    in_read = nullptr;
  }
  ~DiskListStorage() {
    remove(read_storage.c_str());
  }

  void appendData(std::shared_ptr<T> data) override{
    cereal::BinaryOutputArchive out_read(output);
    // PRINT_VAR(data.use_count());
    out_read(*data);
    // PRINT_VAR(data.use_count());
    nb++;
  }
  
  size_t size() const override{
    return nb;
  }

  bool hasData() const override{
    return nb_retrieved <= (int)size();
  }
  void iterate() override{
    if (output.is_open())
      output.close();
    nb_retrieved = 0;
    input = std::ifstream(read_storage, std::ios::binary);
    in_read = new cereal::BinaryInputArchive(input);
    next();
  }
  
  void next() override {
    if (nb_retrieved < (int)size()) {
      T d;
      (*in_read)(d);
      data = std::make_shared<T>(d);
    }
    nb_retrieved++;
    if (nb_retrieved == (int)size())
      delete in_read;
  }

  std::shared_ptr<T> &getCurrentData() override{
    return data;
  }
private:
  size_t nb;
  std::string read_storage;
  std::ifstream input;
  std::ofstream output;
  cereal::BinaryInputArchive *in_read;
  std::shared_ptr<T> data;
  int nb_retrieved;
};

template<class T>
ListStorage<T> *createListStorage(bool use_disk) {
  if (use_disk) {
    return new DiskListStorage<T>();
  }
  return new MemoryListStorage<T> ();
}

#endif

