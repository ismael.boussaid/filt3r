#ifndef KMER_POS_ANALYSER_HPP
#define KMER_POS_ANALYSER_HPP
#include <utility>
#include <vector>
/**
 * Class to deal with kmer positions and retrieve valuable informations on
 * vector<vector<int> >.
 */

class KmerPosAnalyser {
public:
  KmerPosAnalyser(std::vector<std::vector<int> > &positions);

  /**
   * @return the position of the gaps
   * @complexity linear
   */
  std::vector<std::pair<size_t, size_t> > getGaps();

  /**
   * @return compute the distance between the provided value and the closest value
   * at each position in the provided range, and returns the average.
   * `value_pos` is the position at which the given `value` appears.
   */
  float getDistance(int value, size_t start_pos, size_t end_pos,
                    size_t value_pos);

  /**
   * Same but the simple vector on which we compute it is provided
   */
  static float getNonAmbiguousDistance(std::vector<int> &values, int value,
                                       size_t start_pos, size_t end_pos,
                                       size_t value_pos);

  /**
   * @return the first position in the gap, starting from pos and going left.
   * -1 if no such position is found
   */
  int getPreviousGap(size_t pos);

  /**
   * @return the first position in the gap, starting from pos and going right.
   * -1 if no such position is found
   */
  int getNextGap(size_t pos);
private:
  std::vector<std::vector<int> > &positions;
};
#endif
