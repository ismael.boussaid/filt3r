#include <limits>
#include "ReferencePos.hpp"

ReferencePos::ReferencePos():m_ref_cum_length(0), m_ref_pos(0) {}


void ReferencePos::addReference(size_t ref_length, const std::string &name) {
  size_t last = 0;
  if (m_ref_cum_length.size() > 0)
    last = m_ref_cum_length.back();
  m_ref_cum_length.push_back(ref_length + last);
  m_ref_pos.push_back(name_to_pos(name));
}

size_t ReferencePos::getReferenceIndex(size_t pos) const {
  // TODO: Binary search?

  size_t i;
  for (i = 0; i < m_ref_cum_length.size() && pos >= m_ref_cum_length[i]; i++) {}
  return i;
}

size_t ReferencePos::nb() const {
  return m_ref_cum_length.size();
}

std::tuple<string,size_t,int> ReferencePos::getAbsolutePos(size_t rel_pos) const{
  size_t index = getReferenceIndex(rel_pos);
  if (index == nb() || ! isNameDefined(index))
    return make_tuple("", 0, 0);
  
  if (index > 0)
    rel_pos = rel_pos - m_ref_cum_length[index-1];
  size_t pos;
  int strand;
  if (getStrand(index)==1) {
    pos = getStartPos(index) + rel_pos;
    strand = 1;
  } else {
    pos = getStopPos(index) - rel_pos;
    strand = -1;
  }
  return std::make_tuple(getName(index), pos, strand);
}

std::string ReferencePos::getName(size_t index)  const{
  return std::get<0>(m_ref_pos[index]);
}

size_t ReferencePos::getStartPos(size_t index) const {
  return std::get<1>(m_ref_pos[index]);
}

size_t ReferencePos::getStopPos(size_t index) const {
  return std::get<2>(m_ref_pos[index]);
}

bool ReferencePos::getStrand(size_t index) const {
  return std::get<3>(m_ref_pos[index]);
}

bool ReferencePos::isNameDefined(size_t index) const {
  return getStopPos(index) > 0;
}
