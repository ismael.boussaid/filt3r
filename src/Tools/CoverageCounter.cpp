#include "CoverageCounter.hpp"

#include <cmath>
#include <limits>

CoverageCounter::CoverageCounter(size_t len):total_coverage(len){
  for (int i = 0; i < 4; i++)
    nuc_coverage[i] = std::vector<size_t>(len);
  
}

CoverageCounter::CoverageCounter(const CoverageCounter &count):
  total_coverage(count.total_coverage) {
  for (int i = 0; i < 4; i++) {
    nuc_coverage[i] = count.nuc_coverage[i];
  }
}

void CoverageCounter::add(CleanedRead &read, int count) {
  int last_contiguous = -1;
  int size_contiguous = 0;
  size_t ref_pos;
  char nuc;
  size_t len = read.getCleanedPositions().size() + read.getKmerSize() - 1;
  int pos;
  for (size_t i = 0; i < len; i++) {
    if (i < read.getCleanedPositions().size())
      pos = read.getCleanedPositions()[i];
    else
      pos = -1;                  // end of the read
        
    if (pos == -1) {
      if (last_contiguous > -1) {
        // Deal with the remaining of the k-mer
        ref_pos = last_contiguous + size_contiguous + 1;
        size_contiguous++;
        if (size_contiguous == read.getKmerSize()-1) {
          size_contiguous = 0;
          last_contiguous = -1;
        }
      } else {
        // undefined position
        ref_pos = std::numeric_limits<size_t>::max();
        last_contiguous = -1;
      }
    } else {
      ref_pos = pos;
      last_contiguous = pos;
      size_contiguous = 0;
    }
    if (ref_pos != std::numeric_limits<size_t>::max()) {
      nuc = read.getFilteredRead()->getSequence()[i];
      if (count < 0) {
        count = -min(nuc_coverage[nuc_to_int(nuc)][ref_pos], (size_t)-count);
        count = -min((size_t)-count, total_coverage[ref_pos]);
        // prevent having negative values when doing += count
      }
      total_coverage[ref_pos] += count;
      nuc_coverage[nuc_to_int(nuc)][ref_pos] += count;
    }
  }
}

size_t CoverageCounter::get(size_t pos) const {
  return total_coverage[pos];
}

char CoverageCounter::get(size_t pos, float mt_ratio, float error) const {
  float diff_min = 2;
  int nuc = -1;
  size_t max = 0;
  int max_nuc = -1;
  if (get(pos) == 0)
    return 'N';
  for (int i = 0; i < 4; i++) {
    float nuc_cov = nuc_coverage[i][pos]*1./total_coverage[pos];
    float diff = fabs(nuc_cov - mt_ratio);
    if (diff < diff_min && mt_ratio >= nuc_cov/error && mt_ratio <= nuc_cov*error
        && nuc_coverage[i][pos] > 0) {
      diff_min = diff;
      nuc = i;
    }
    if (nuc_coverage[i][pos] > max) {
      max = nuc_coverage[i][pos];
      max_nuc = i;
    }
  }
  if (nuc == -1)
    nuc = max_nuc;
  char dna[] = {'A', 'C', 'G', 'T'};
  return dna[nuc];
}

size_t CoverageCounter::getAverage(int pos1, int pos2) const {
  size_t counts = 0;
  int nb = 0;
  if (pos1>=0) {
    counts += total_coverage[pos1];
    nb++;
  }
  if ((size_t) pos2 < total_coverage.size()) {
    counts += total_coverage[pos2];
    nb++;
  }
  if (nb>0)
    return counts/nb;
  return 0;
}

std::array<size_t, 4> CoverageCounter::nuc_counts(size_t pos) const {
  std::array<size_t, 4> counts;
  for (int i = 0; i < 4; i++) {
    counts[i] = nuc_coverage[i][pos];
  }
  return counts;
}

size_t CoverageCounter::size() const {
  return total_coverage.size();
}

size_t CoverageCounter::operator[](size_t pos) const {
  return get(pos);
}

std::ostream &operator<<(std::ostream &out, CoverageCounter &cc) {
  size_t n = cc.size();
  for (size_t i = 0; i < n; i++)
    out << " " << cc[i] << std::endl;
  return out;
}
