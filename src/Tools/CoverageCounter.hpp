#ifndef COVERAGE_COUNTER_HPP
#define COVERAGE_COUNTER_HPP

#include <array>
#include <vector>
#include <Class/Cleaning/CleanedRead.hpp>

class CoverageCounter {
private:
  std::vector<size_t> total_coverage;
  std::vector<size_t> nuc_coverage[4];

public:
  CoverageCounter(size_t len);
  CoverageCounter(const CoverageCounter &count);

  /**
   * Add to the coverage the read in parameter with the provided count
   */
  void add(CleanedRead &read, int count=1);

  /**
   * Get the number of reads covering that position
   */
  size_t get(size_t pos) const;
  /**
   * Get the nucleotide at the given position which is the closest from the provided ratio (but still existing)
   * (specify 1 to get the most abundant).
   * @param pos: position on the reference
   * @param mt_ratio: ratio the nucleotide should reach
   * @param error: factor by which we allow a difference, otherwise will return the max quantity
   */
  char get(size_t pos, float mt_ratio, float error=2) const;

  /**
   * @return the average coverage at the two positions.
   * If one of them is not defined, it is ignored. If both are undefined, it returns 0.
   */
  size_t getAverage(int pos1, int pos2) const;

  std::array<size_t, 4> nuc_counts(size_t pos) const;

  size_t size() const;
  
  /**
   * Same as get()
   */
  size_t operator[](size_t pos) const;
};

std::ostream &operator<<(std::ostream &out, CoverageCounter &cc);
#endif
