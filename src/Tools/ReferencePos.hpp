#ifndef REFERENCE_POS_HPP
#define REFERENCE_POS_HPP

#include <string>
#include <vector>
#include <Tools/filter_tools.hpp>

class ReferencePos {
private:
  vector<size_t> m_ref_cum_length;                              // Cumulative length of each ref
  vector<ref_pos_t> m_ref_pos;                                    // Positions of the references
  
public:
  ReferencePos();

  void addReference(size_t ref_length, const std::string &name);

  /**
   * Return the index of the reference (between 0 and nb() -1) for the given position
   */
  size_t getReferenceIndex(size_t pos) const;

  size_t nb() const;

  /**
   * @return the absolute position on the reference corresponding  to the provided relative position (on the same strand)
   */
  std::tuple<string,size_t,int> getAbsolutePos(size_t rel_pos) const;
  
  /**
   * @return the name of the reference at index `index` (between 0 and nb()-1)
   */
  std::string getName(size_t index) const;
  /**
   * @return the starting position of the reference at index `index` (between 0 and nb()-1)
   */
  size_t getStartPos(size_t index) const;
  /**
   * @return the stop position of the reference at index `index` (between 0 and nb()-1)
   */
  size_t getStopPos(size_t index) const;
  /**
   * @return the strand of the reference at index `index` (between 0 and nb()-1), true for forward strand and false for reverse strand
   */
  bool getStrand(size_t index) const;

  bool isNameDefined(size_t index) const;
};

#endif
