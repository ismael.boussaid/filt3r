#include "filter_tools.hpp"

#include <cmath>

std::string bool_to_string(bool b) {
  return (b) ? "true" : "false";
}

std::list<std::string> getModifiedSequences(std::string &seq, size_t pos) {
  std::list<std::string> modified;
  for (char nuc: {'A', 'C', 'G', 'T' }) {
    std::string modification = seq;
    modification[pos] = nuc;
    modified.push_back(modification);
  }
  return modified;
}

std::vector<std::pair<size_t, size_t> > get_cleaned_gaps(std::vector<int> &positions) {
  std::vector<std::pair<size_t, size_t> > results;
  size_t start_pos;
  bool is_in_N = false;

  size_t i = 0;
  for (auto &pos: positions) {
    if (pos == -1) {
      if (! is_in_N) {
        is_in_N = true;
        start_pos = i;
      }
    } else {
      if (is_in_N) {
        is_in_N = false;
        results.push_back(std::make_pair(start_pos, i-1));
      }
    }
    i++;
  }
  if (is_in_N)
    results.push_back(std::make_pair(start_pos, i-1));
  return results;
}

ref_pos_t name_to_pos(const std::string &name) {
  // name:start_pos-end_pos:strand

  size_t pos_name, pos_strand, pos_sep_pos;

  pos_name = name.find(":");

  if (pos_name != std::string::npos) {
    pos_strand = name.find(":", pos_name+1);

    if (pos_strand != std::string::npos) {
      pos_sep_pos = name.find("-", pos_name+1);

      if (pos_sep_pos != std::string::npos && pos_sep_pos < pos_strand) {
        try {
          string ref_name = name.substr(0, pos_name);
          size_t pos_start = std::stoi(name.substr(pos_name+1, pos_sep_pos - pos_name - 1));
          size_t pos_end = std::stoi(name.substr(pos_sep_pos+1, pos_strand - pos_sep_pos - 1)) ;
          bool strand = std::stoi(name.substr(pos_strand+1, name.size() - pos_strand - 1)) == 1;
          if (pos_start < pos_end)
            return std::make_tuple(ref_name, pos_start, pos_end, strand);
        } catch(Exception &e) {}
      }
    }
  }

  return std::make_tuple("", 0, 0, true);
}

string options_to_str(IProperties *options) {
  string options_str = "";
  bool first = true;
  for (auto key: options->getKeys()) {
    if (! first)
      options_str+=" ";
    first = false;
    options_str += key+" "+options->getStr(key);
  }
  return options_str;
}

char complement_nucleotide(char nuc) {
  switch(nuc) {
  case 'A': case 'a': return 'T';
  case 'C': case 'c': return 'G';
  case 'G': case 'g': return 'C';
  case 'T': case 't': return 'A';

  case 'Y': case 'y': return 'R'; // pyrimidine (CT)
  case 'R': case 'r': return 'Y'; // purine (AG)
  case 'W': case 'w': return 'W'; // weak (AT)
  case 'S': case 's': return 'S'; // strong (GC)
  case 'K': case 'k': return 'M'; // keto (TG)
  case 'M': case 'm': return 'K'; // amino (AC)

  case 'B': case 'b': return 'V'; // not A
  case 'D': case 'd': return 'H'; // not C
  case 'H': case 'h': return 'D'; // not G
  case 'V': case 'v': return 'B'; // not T

  case 'N': case 'n': return 'N';

  case ' ': return ' '; // ne devrait pas arriver...

  default: return '?';
  }
}

string revcomp(const string &dna, bool do_revcomp) {
  if (!do_revcomp)
    return dna;

  string rcomp(dna);
  for (size_t i = 0; i < dna.length(); i++) {
    rcomp[dna.length() - i - 1] = complement_nucleotide(dna[i]);
  }

  return rcomp;
}

string to_string_nan(double d) {
  if (isnan(d))
    return "NaN";
  return to_string(d);
}

