#ifndef FILTER_TOOLS_H
#define FILTER_TOOLS_H
#include <list>
#include <string>
#include <iostream>
#include <vector>
#include <gatb/gatb_core.hpp>

#define PRINT_VAR(v) std::cerr << #v << " = " << v << std::endl

typedef std::tuple<std::string, size_t, size_t, bool> ref_pos_t;

std::string bool_to_string(bool b);

/**
 * List all the modified sequnences when modifying a nucleotide at position pos.
 * `seq` is among the returned k-mers
 */
std::list<std::string> getModifiedSequences(std::string &seq, size_t pos);

std::vector<std::pair<size_t, size_t> > get_cleaned_gaps(std::vector<int> &positions);

/**
 * @param nuc is A, C, G, T or any extended nucleotide (or lowercase)
 * @return the complementary nucleotide of nuc
 * @copyright Copied from Vidjil
 */
char complement_nucleotide(char nuc);

inline int nuc_to_int(char nuc) {
  // A/a : 01*0 0001
  // C/c : 01*0 0011
  // G/g : 01*0 0111
  // T/t : 01*1 0100
  // pos :    3210
  // Bit de poids fort : b_2
  // Bit de poids faible : xor entre b_2 et b_1
  return ((nuc & 4) >> 1) // poids fort
    | (((nuc & 4) >> 2) ^ ((nuc & 2) >> 1));
}

/**
 * Return the position associated to the name.
 * @pre name must be correctly formated: name:start_pos-end_pos:strand
 *      where start_pos < end_pos and strand is either 1 or -1.
 * @return the corresponding position or <"",0,0,true> if the format is not respected
 */
ref_pos_t name_to_pos(const std::string &name);

template<typename T>
std::string vector_to_json(std::vector<T> &v);

template <typename T>
std::string vector_to_json(std::vector<T> &v) { 
  std::string out = "[";
  bool start = true;
  for (int i: v) {
    if (! start)
      out += ", ";
    start = false;
    if (i == -1)
      out += "null";
    else
      out += std::to_string(i);
  }
  out += "]";
  return out;
}

string options_to_str(IProperties *options);

/**
 * @return reverse(complement(dna)) if do_revcomp, otherwise dna
 * @copyright Copied from Vidjil
 */
string revcomp(const string &dna, bool do_revcomp);

/**
 * Return "NaN" if NaN otherwise return to_string of the double
 */
string to_string_nan(double);
#endif
