#include <algorithm>
#include <memory>
#include <cstring>
#include "Class/Filter/ClassFilter.hpp"
#include "Class/Filter/FilteredReads.hpp"
#include "Class/Cleaning/Cleaning.hpp"
#include "Class/Breakpoint/ClassBreakpoint.hpp"
#include <Tools/ListStorage.hpp>

#ifndef GIT_SHA1
#define GIT_SHA1 "???"
#endif

#define BLOOM_SIZE 100000
#define NB_HASH 3
#define NB_KMER_EXTENSION 2
#define PERCENTAGE_SIM_FLT3 .3
#define DEFAULT_NB_THREADS 1
#define STR_KMER                        "-k"
#define STR_URI_SEQUENCES               "--sequences"
#define STR_TRANSCRIPT                  "--ref"
#define STR_BLOOMSIZE                   "--bloom-size"
#define STR_KMER_EXTENSION              "--kmer-extend"
#define STR_NBHASH_BLOOM                "--nb-hash"
#define STR_PERCENTAGE_SIMILARITY_FLT3  "--threshold"
#define STR_DUPLICATION_MIN_QUANTITY    "--min-nb-duplication"
#define STR_HIGHEST_FP_RATE             "--highest-fp-rate"
#define STR_IGNORE_SHORT_INDELS         "--ignore-short-indels"
#define STR_OUTPUT_FILE                 "--out"
#define STR_ON_DISK                     "--low-mem"
#define STR_DEBUG    "--debug"
#define STR_OUT_SEQUENCES "--out-sequences" 
#define STR_NO_BLOOM      "--no-bloom-filter"
#define STR_NO_SUBST_REMOVAL "--no-subst-removal"
#define STR_FILTER_ONLY                 "--only-filter"
#define STR_NB_THREADS                  "--nb-threads"
#define STR_VCF                         "--vcf"

#define FILTRE_DUPLICATION_OCCURENCE_1  2
#define DISPLAY_SEQUENCE                0
#define DISPLAY_SEQUENCE_BEFORE_CLEAN   0



void json_output(std::map<std::string, Breakpoint*> &mapResult, std::string output_file, u_int64_t total, int nbSeqence, int kmer_size, IProperties* options, bool debug) {
  std::cerr << "Writing results to " << output_file << std::endl;
  ofstream m_resultFile;
  m_resultFile.open (output_file);
  std::string data = "{\"command line\": \""+options_to_str(options)+"\",\n";
  data += "\"version\" : \"" GIT_SHA1 "\",\n";
  data += "\"nb_total\" : "+std::to_string(total)+",\n";
  data += "\"nb_filtered\" : " + std::to_string(nbSeqence) + ",\n ";

  int nbSequenceDefectueuse = 0;
  for(std::map<std::string, Breakpoint*>::iterator i=mapResult.begin(); i!=mapResult.end(); ++i) {
    nbSequenceDefectueuse += i->second->count();
  }
  data += "\"nb_reads_in_events\" : " + std::to_string(nbSequenceDefectueuse) + ",\n ";
  data += "\"details\" : \n[\n";
  bool first = true;
  for(std::map<std::string, Breakpoint*>::iterator i=mapResult.begin(); i!=mapResult.end(); ++i) {
    if (! first)
      data += ",\n";
    first = false;
    data += i->second->exportJSON(debug);
  }
  data += "],\n \"percentage\" : " + std::to_string((double)nbSequenceDefectueuse/nbSeqence*100) + "}";
  m_resultFile << data;
  m_resultFile.close();
}

void vcf_output(std::map<std::string, Breakpoint*> &mapResult, std::string output_file, IProperties* options, bool debug) {
  std::cerr << "Writing VCF to " << output_file << std::endl;
  ofstream m_resultFile;
  IBank* reference = Bank::open (options->getStr(STR_TRANSCRIPT));
  Iterator<Sequence> *itRef = reference->iterator();
  std::vector<std::string> references;
  for (itRef->first(); !itRef->isDone(); itRef->next()) {
    references.push_back((*itRef)->toString());
  }

  m_resultFile.open (output_file);
  m_resultFile << "##fileformat=VCFv4.1" << std::endl
		<< "##INFO=<ID=M,Number=.,Type=String,Description=\"Number of reads containing the variant\">" << std::endl
		<< "##INFO=<ID=WT,Number=.,Type=String,Description=\"Number of wild type reads\">" << std::endl
		<< "##INFO=<ID=M/WT,Number=.,Type=String,Description=\"Allelic ratio (M/WT)\">" << std::endl
		<< "##INFO=<ID=VAF,Number=.,Type=String,Description=\"Variant Allele Frequency (ratio)\">" << std::endl
                << "##sample=<" << options->getStr(STR_URI_SEQUENCES) << ">" << std::endl
                << "#CHROM POS ID REF ALT QUAL INFOS" << std::endl;

  
  for(std::map<std::string, Breakpoint*>::iterator i=mapResult.begin(); i!=mapResult.end(); ++i) {
    std::string line = i->second->exportVCF(references, debug);
    if (line.size() >0)
      m_resultFile <<  line << std::endl;
  }
  m_resultFile.close();

}

int main(int argc, char* argv[]) {
  /** We create a command line parser. */
  OptionsParser parser ("FiLT3r");
  parser.push_back (new OptionOneParam (STR_KMER, "kmer size",           true));
  parser.push_back (new OptionOneParam (STR_URI_SEQUENCES, "Input raw sequences (FASTA, FASTQ, potentially gzipped). Multiple files can be given, separated by a comma, without any space between them.", true));
  parser.push_back (new OptionOneParam (STR_TRANSCRIPT, "Reference sequence", true));
  parser.push_back (new OptionOneParam (STR_BLOOMSIZE, "Size of the Bloom filter", false, to_string(BLOOM_SIZE)));
  parser.push_back (new OptionOneParam (STR_NBHASH_BLOOM, "Number of hash functions (if 0 the optimal number of hash functions for the false positive rate is chosen, however this number of hash functions may not be optimal for the running time!)", false, to_string(NB_HASH)));
  parser.push_back (new OptionOneParam (STR_NB_THREADS, "Number of threads", false, to_string(DEFAULT_NB_THREADS)));
  parser.push_back (new OptionOneParam (STR_OUTPUT_FILE, "Output filename. If the filename is empty the output will be written in a file whose name is the same as the input with an appended extension '.results.json'", false));
  parser.push_back (new OptionNoParam (STR_VCF, "Output an additional VCF file. The output name will be the same as the one for " STR_OUTPUT_FILE ", but the .json extension will be replaced with .vcf (or .vcf will be appended if no such .json exists)", false, false));
  parser.push_back (new OptionOneParam (STR_PERCENTAGE_SIMILARITY_FLT3, "Ratio of k-mers from a read that should match the reference to be fully analysed (all the others are discarded).", false, to_string(PERCENTAGE_SIM_FLT3)));
  parser.push_back (new OptionOneParam (STR_KMER_EXTENSION, "number of k-mers needed to confirm a breakpoint", false, to_string(NB_KMER_EXTENSION)));
  parser.push_back (new OptionOneParam (STR_DUPLICATION_MIN_QUANTITY, "Minimal number of identical duplication kept (default is " + to_string(FILTRE_DUPLICATION_OCCURENCE_1) + ", 1 for all)", false, to_string(FILTRE_DUPLICATION_OCCURENCE_1)));
  parser.push_back (new OptionOneParam (STR_IGNORE_SHORT_INDELS, "maximal length of the indels to ignore (>=0), 0 to take them all", false, "0"));
  parser.push_back (new OptionNoParam (STR_ON_DISK, "Use disk rather than main memory to store reads with duplications: FiLT3r main memory usage will almost be independent on the number of breakpoints found.", false, false));
  parser.push_back (new OptionNoParam (STR_OUT_SEQUENCES, "output sequences", false, false));
  parser.push_back (new OptionNoParam (STR_DEBUG, "debug mode", false, false));
  parser.push_back (new OptionNoParam (STR_FILTER_ONLY, "Only filter reads and stop after that. In such a case the parameter " STR_OUTPUT_FILE " defined the filename for the filtered reads (the output file is a gzipped FASTQ file).", false, false));

  IOptionsParser* xp_options = new OptionsParser ("experimental");
  parser.push_back(xp_options);

  xp_options->push_back(new OptionNoParam (STR_NO_BLOOM, "don't use Bloom filter for filtering", false, false));
  xp_options->push_back(new OptionNoParam (STR_NO_SUBST_REMOVAL, "don't remove substitutiosn", false, false));
  xp_options->push_back (new OptionOneParam (STR_HIGHEST_FP_RATE, "Maximal allowable false positivity rate for the Bloom filter", false, to_string(HIGHEST_FP_RATE)));
  
  try
  {
      /** We parse the user options. */
      IProperties* options = parser.parse (argc, argv);
      int kmerSize = options->getInt(STR_KMER);
      int nbThreads = options->getInt(STR_NB_THREADS);
      std::string sequencesFile = options->getStr(STR_URI_SEQUENCES);
      std::string output_filename;
      string transcript = options->getStr(STR_TRANSCRIPT);
      int bloomSize = options->getInt(STR_BLOOMSIZE);
      int nbHash_bloom = options->getInt(STR_NBHASH_BLOOM);
      int kmer_extension = options->getInt(STR_KMER_EXTENSION);
      int minDuplications = options->getInt(STR_DUPLICATION_MIN_QUANTITY);
      float percentage_similarity_flt3 = options->getDouble(STR_PERCENTAGE_SIMILARITY_FLT3);
      double highest_fp_rate = options->getDouble(STR_HIGHEST_FP_RATE);
      bool create_vcf = options->get(STR_VCF) != 0;
      bool debug = options->get(STR_DEBUG) != 0;
      bool out_sequences = options->get(STR_OUT_SEQUENCES) != 0;
      bool use_bloom = options->get(STR_NO_BLOOM) == 0;
      bool use_disk = options->get(STR_ON_DISK) != 0;
      bool remove_subst = options->get(STR_NO_SUBST_REMOVAL) == 0;
      bool only_filter = options->get(STR_FILTER_ONLY) != 0;
      int ignore_short_indels = options->getInt(STR_IGNORE_SHORT_INDELS);

      if (! options->get(STR_OUTPUT_FILE)) {
        size_t pos = sequencesFile.find(",");
        if (pos != string::npos)
          output_filename = sequencesFile.substr(0, pos);
        else
          output_filename = sequencesFile;
        if (only_filter)
          output_filename += ".filtered.fastq.gz";
        else
          output_filename += ".results.json";
      } else {
        output_filename = options->getStr(STR_OUTPUT_FILE);
      }
      
      // étape 1 : Filtrer les données
      std::cerr << "Filtering..." << std::endl;
      std::shared_ptr<ClassFilter> filtre(new ClassFilter(kmerSize, transcript, sequencesFile, nbThreads, use_disk));
      filtre->setHighestFPRate(highest_fp_rate);

      ListStorage<FilteredRead> *filtered = filtre->filter(percentage_similarity_flt3, bloomSize, nbHash_bloom, use_bloom, (only_filter) ? output_filename : "");
      //filtre.displayResult();

      std::cerr << "Filtered " << filtre->nbSequencesFLT3 << " sequences (" << filtre->nbSequencesFLT3Rev << " reversed) among " << filtre->nbSequences<< endl << endl;

      if (only_filter) {
        return 0;
      }
      
      // étape 2 : nettoyage des données
      Cleaning* clean = new Cleaning(filtered, filtre.get(), kmer_extension, remove_subst, true, use_disk);
      // Cleaning* clean = new Cleaning(filtered);
      if (debug) {
        std::cerr << *clean << std::endl;
      }

      // étape 3 : recherche des points d'arrets
      ClassBreakpoint* breakpoint = new ClassBreakpoint(clean->cleaned, filtre->getReference(), use_disk);
      breakpoint->searchBreakpoints(kmer_extension, minDuplications,
                                    filtre->getReferencePos(), ignore_short_indels);
      int size = clean->cleaned->size();

      

      json_output(breakpoint->getResults(), output_filename, filtre->nbSequences, size, kmerSize, options, out_sequences);
      if (create_vcf) {
	std::string output_vcf = output_filename;
	if (output_filename.length() >= 5 && output_filename.substr(output_filename.length() - 5, 5) == ".json") {
	  output_vcf = output_filename.substr(0, output_filename.length()-5);
	}
	output_vcf += ".vcf";
	vcf_output(breakpoint->getResults(), output_vcf, options, out_sequences);
      }
      delete clean;
      delete breakpoint;
      delete filtered;


      std::cerr << "Done." << std::endl;

    }
    catch (OptionFailure& e)
    {
        return e.displayErrors (std::cerr);
    }
    catch (Exception& e)
    {
        std::cerr << "EXCEPTION: " << e.getMessage() << std::endl;
    }

}
