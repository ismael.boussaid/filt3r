#ifndef FILTERED_READS_H
#define FILTERED_READS_H
#include <vector>
#include <gatb/gatb_core.hpp>
#include <istream>
#include <ostream>

#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>

/**
 * Stores information on a filtered read and its similiraties with FLT3 gene
 */
class FilteredRead {

private:
  // Stores the positions of the k-mers on the reference sequence 
  std::vector<std::vector<int> > positions;
  string sequence;
  bool reversed;
  std::vector<int> all_positions;
public:

  FilteredRead();
  ~FilteredRead();
  FilteredRead(string seq, bool reversed);

  /**
   * Add a matching k-mer at a given position in the sequence.
   * It appends to a potentially existing entry
   * @param pos : position in the sequence
   * @param ref_pos : position in the reference sequence with this k-mer
   */
  void addMatchingKmer(size_t pos, int ref_pos);
  /**
   * Sets the list of matching k-mers at a given position in the sequence.
   * It replaces a potentially existing entry
   * @param pos : position in the sequence
   * @param ref_pos : list of positions in the reference sequence with this k-mer
   */
  void addMatchingKmers(size_t pos, std::vector<int> &ref_pos);

  /**
   * Call to partially clean the object (ie. remove all_positions)
   */
  void clean();

  string &getSequence();
  /**
   * @return the number of k-mers that are repeated at least min_repeats times
   * @complexity O(n log n), with n the number of matched k-mers
   */
  size_t getNbRepeatedKmers(size_t min_repeats);
  std::vector<int> &getMatchingPositions(size_t pos);
  std::vector<std::vector<int > > &getPositions();
  bool isReversed() const;

  
  template <class Archive>
  void serialize(Archive &ar) {
    ar(positions, sequence, reversed, all_positions);
  }    

  size_t size() const;
  
};

std::ostream &operator<<(ostream &out, FilteredRead &);
#endif
