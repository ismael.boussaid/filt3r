#ifndef CLASS_FILTER_HPP
#define CLASS_FILTER_HPP

#include <gatb/gatb_core.hpp>
#include <string>
#include <cstring>
#include <memory>
#include <vector>

class ClassFilter;

#include <Tools/filter_tools.hpp>
#include <Tools/ReferencePos.hpp>
#include <Tools/ListStorage.hpp>
#include "FilteredReads.hpp"

#define HIGHEST_FP_RATE .1

typedef Bloom<Kmer<>::Type> bloom_type;

class ClassFilter
{
    public:
    ClassFilter(int kmerSize, std::string bankTranscript, std::string bankSequences,
                int nbThreads, bool use_disk=false);
    ~ClassFilter();

  /**
   * Filter sequences
   *
   * @param p: proportion of k-mers that must match
   * @param bloomSize: number of bits for the bloom filter
   * @param nhash: number of hash functions for the bloom filter
   * @param use_bloom: whether to atually use a bloom filter or not (false for benchmarking mainly)
   * @param outname: filename for the filtered reads (if empty the reads are stored in 
   *        memory and returned by the function)
   */
  ListStorage<FilteredRead> *filter(float p, int bloomSize, int nhash, bool use_bloom, string outname);

  /**
   * Returns the position of the provided kmer
   */
  std::vector<int> getPositions(string kmer);

  /**
   * @returns the reference sequence
   */
  std::string getReference() const;

  /**
   * @returns the size of the reference sequence
   */
  size_t getReferenceLength() const;

  ReferencePos getReferencePos();

  int getKmerSize() const;

  /**
   * Sets the highest false positivity rate above which we abort the use of a Bloom filter.
   */
  void setHighestFPRate(double highest);
    private:
    bool use_disk;
    std::string m_bankTranscript;
    std::string m_bankSequences;
    int m_kmerSize;
    u_int64_t nbThreads;
    double highest_fp_rate;
    bloom_type *m_bloom;        // Contains k-mers on both strands
    bloom_type *m_bloom_rev;    // Only reverse k-mers
  std::map<std::string, std::vector<int> > m_hashMapTranscript;// TODO don't have strings as keys
    std::string m_ref;
    size_t m_ref_length;                                       // Total length of all the references
    ReferencePos m_ref_pos;

    /**
     * Create a bloom filter of the specified size with the given number of hash functions.
     * If nhash is 0 the optimal number of hash functions is computed.
     * If the false positive rate is too high Bloom filter usage is deactivated.
     * @return true iff the bloom filter was properly built
     */
    bool CreateBloomFilter(int bloomSize, int nhash);
    void CreateHashMap();
  size_t nbMatchedKmers(Sequence &itSeq, Kmer<>::ModelCanonical::Iterator & itKmer,
                        bloom_type *bloom, size_t &nbKmers);
public:
    u_int64_t nbSequences, nbSequencesFLT3, nbSequencesFLT3Rev;
};

#endif
