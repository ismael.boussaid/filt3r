#include "FilteredReads.hpp"
#include <Tools/filter_tools.hpp>
#include <string>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/memory.hpp>

FilteredRead::FilteredRead():positions(0),sequence(),reversed(false), all_positions(0){}

FilteredRead::~FilteredRead(){}

FilteredRead::FilteredRead(string s, bool reversed):positions(s.size()),sequence(s), reversed(reversed), all_positions(){
  all_positions.reserve(s.size());
}

void FilteredRead::addMatchingKmer(size_t pos, int ref_pos) {
  assert(pos < positions.size());
  
  positions[pos].push_back(ref_pos);
  all_positions.push_back(ref_pos);
}

void FilteredRead::addMatchingKmers(size_t pos, vector<int> &ref_pos) {
  assert(pos < positions.size());
  
  positions[pos] = ref_pos;
  for (int pos: ref_pos) {
    all_positions.push_back(pos);
  }
}

void FilteredRead::clean() {
  all_positions.clear();
}

string &FilteredRead::getSequence() {
  return sequence;
}

size_t FilteredRead::getNbRepeatedKmers(size_t min_repeats) {
  size_t length_repeat = 0;
  size_t nb_identical_kmers = 0;
  if (all_positions.size() == 0)
    return 0;
  std::sort(all_positions.begin(), all_positions.end());
  for (size_t i = 0; i < all_positions.size()-1; i++) {
    if (all_positions[i] == all_positions[i+1]) {
      if (length_repeat == 0)
        length_repeat++;
      length_repeat++;
    } else {
      if (length_repeat >= min_repeats)
        nb_identical_kmers += length_repeat;
      length_repeat = 0;
    }
  }
  if (length_repeat >= min_repeats)
    nb_identical_kmers += length_repeat;

  return nb_identical_kmers;
}

vector<int> &FilteredRead::getMatchingPositions(size_t pos) {
  assert(pos < positions.size());
  return positions[pos];
}

vector<vector<int> > &FilteredRead::getPositions() {
  return positions;
}

bool FilteredRead::isReversed() const {
  return reversed;
}

size_t FilteredRead::size() const {
  return positions.size();
}

std::ostream &operator<<(ostream &out, FilteredRead &r) {
  out << r.getSequence() << std::endl;
  for (size_t i = 0; i < r.size(); i++) {
    std::vector<int> v = r.getMatchingPositions(i);
    if (v.size() == 0) {
      out << ".";
    } else if (v.size() == 1) {
      out << v[0];
    } else {
      out << "[";
      for (int k: v) {
        out << k << ",";
      }
      out << "]";
    }
    out << " ";
  }
  return out;
}
