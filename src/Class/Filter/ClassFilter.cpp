#include "ClassFilter.hpp"
#include<memory>
#include<cmath>

using namespace std;

static const size_t span = KMER_SPAN(0);

/**
Classe permettant de réaliser l'étape 1 c'est à dire de filtrer
l'ensemble de séquences afin de ne garder que les gènes FLT3.
*/
ClassFilter::ClassFilter(int kmerSize, string bankTranscript, string bankSequences, int nbThreads, bool use_disk)
{
  this->use_disk = use_disk;
  m_kmerSize = kmerSize;
  m_bankTranscript = bankTranscript;
  m_bankSequences = bankSequences;
  m_bloom = nullptr;
  m_bloom_rev = nullptr;
  m_ref_length = 0;
  m_ref_pos = ReferencePos();
  nbSequences = 0;
  nbSequencesFLT3 = 0;
  nbSequencesFLT3Rev = 0;
  this->nbThreads = nbThreads;
  highest_fp_rate = HIGHEST_FP_RATE;
  CreateHashMap();
}

ClassFilter::~ClassFilter() {
  if (m_bloom != nullptr)
    delete m_bloom;
  if (m_bloom_rev != nullptr)
    delete m_bloom_rev;
}

bool ClassFilter::CreateBloomFilter(int bloomsize, int nhash)
{
  IBank* transcript = Bank::open (m_bankTranscript);
  LOCAL (transcript);
  int estimateNbItems = transcript->estimateSequencesSize()*2;

  if (nhash == 0)
    nhash = round(bloomsize * log(2) / estimateNbItems);
  
  m_bloom = new bloom_type(bloomsize,nhash);
  m_bloom_rev = new bloom_type(bloomsize,nhash);

  double fp_proba = pow(1 - exp(-nhash*estimateNbItems*1./bloomsize), nhash);
  
  std::cerr << "Bloom filter of " << bloomsize << " bits created with " << nhash << " hash functions (proba of false positive: "  << fp_proba << ")" << std::endl;
  if (fp_proba > highest_fp_rate) {
    std::cerr << "WARNING: The false positive rate is too high. Bloom filter will be deactivated. If you want to still use a Bloom filter, change the highest false positivity rate with --highest-fp-rate." << std::endl;
    return false;
  }
  // We declare a kmer model with a given span size.
  Kmer<span>::ModelCanonical model (m_kmerSize);

  // We declare an iterator on a given sequence.
  Kmer<span>::ModelCanonical::Iterator itKmer (model);

  // We create an iterator over this bank.
  Iterator<Sequence> *itFLT3 = transcript->iterator();

  m_ref_length = 0;
  // We loop over sequences.
  for (itFLT3->first(); !itFLT3->isDone(); itFLT3->next())
  {
      // We set the data from which we want to extract kmers.
    itKmer.setData ((*itFLT3)->getData());
    m_ref_length += (*itFLT3)->getDataSize();
    m_ref_pos.addReference((*itFLT3)->getDataSize(), (*itFLT3)->getComment());
      // We iterate the kmers.
      for (itKmer.first(); !itKmer.isDone(); itKmer.next())
      {
          m_bloom->insert(itKmer->forward());
          m_bloom->insert(itKmer->revcomp());
          m_bloom_rev->insert(itKmer->revcomp());
      }
  }

  delete itFLT3;
  return true;
}


/**
Permer de créer une table de hashage contenant les kmers
dans la variable m_hashMapTranscript
clé (String) : kmer
valeur (int) : position dans le gène FLT3séquence et des


exemple:
Si on a la séquence suivante ACGTCCC et des kmer de taille 3
alors la table de hashage sera la suivante:
{
"ACG" : 1,
"CGT" : 2,
"GTC" : 3,
"TCC" : 4,
"CCC" : 5
}
*/
void ClassFilter::CreateHashMap()
{
  if (m_bankTranscript.length() == 0)
    return;

  int position = 0;

  IBank* transcript = Bank::open (m_bankTranscript);
  LOCAL (transcript);
  m_ref_length = transcript->getSize();

  // We declare a kmer model with a given span size.
  Kmer<span>::ModelCanonical model (m_kmerSize);

  // We declare an iterator on a given sequence.
  Kmer<span>::ModelCanonical::Iterator itKmer (model);

  // We create an iterator over this bank.
  Iterator<Sequence> *itFLT3 = transcript->iterator();

  // We loop over sequences.
  for (itFLT3->first(); !itFLT3->isDone(); itFLT3->next())
  {
      // We set the data from which we want to extract kmers.
    itKmer.setData ((*itFLT3)->getData());
    m_ref += (*itFLT3)->toString()+";";

      // We iterate the kmers.
      for (itKmer.first(); !itKmer.isDone(); itKmer.next())
      {
        list<string> kmers = {model.toString(itKmer->forward())};
        for (auto km: kmers) {
          if(m_hashMapTranscript.count(km) == 0) {
            m_hashMapTranscript[km] = std::vector<int>();
          }
          m_hashMapTranscript[km].push_back(position);
        }
        position++;
      }
  }

  delete itFLT3;
}

size_t ClassFilter::nbMatchedKmers(Sequence &seq, Kmer<>::ModelCanonical::Iterator &itKmer,
                                   bloom_type *bloom, size_t &nbKmers) {
  if (bloom == nullptr)
    return 0;
  size_t nbPresence = 0;
  // We set the data from which we want to extract kmers.
  itKmer.setData (seq.getData());
  // We iterate the kmers.
  for (itKmer.first(); !itKmer.isDone(); itKmer.next()) {
    nbKmers++;
    if(bloom->contains(itKmer->forward())) {
      nbPresence++;
    }
  }
  return nbPresence;
}

/**
Permet de filtrer un fichier de sequences en ne gardant
seulement les séquences FLT3.
une séquences correspond au gène FLT3 si la ressemblance
dépasse p.

param p : le taux minimum de ressemblance pour dire qu'une
          sequance est un gène FLT3.

On va vérifier si chaque séquence correspond au gène FLT3
grâce au Filtre de Bloom, si la ressemblance est validé
on va écrire dans un fichier la séquence sous la forme suivante:
Pour chaque kmer de cette séquence on va écrire N si le kmer
n'est pas présent dans le gène FLT3 sinon la position de ce
kmer dans le gène.

exemple de fichier en sorti:
N N N 1 2 3 4 5 N N N 6 8 N N N ...
*/
ListStorage<FilteredRead> *ClassFilter::filter(float p, int bloomsize, int nhash, bool use_bloom, const string outname)
{
  if (use_bloom)
    use_bloom = CreateBloomFilter(bloomsize, nhash);
  if (! use_bloom)
    // This is not in the else as the construction can deactivate the Bloom filter.
    std::cerr << "WARNING: Bloom filter is deactivated, the program will be slower" << std::endl;
  IBank* sequences = Bank::open (m_bankSequences);
  LOCAL (sequences);

  // We declare a kmer model with a given span size.
  Kmer<span>::ModelCanonical model (m_kmerSize);

  std::unique_ptr<IBank> out;
  if (outname.size() > 0) {
    out.reset(new BankFasta(outname, true, true));
  }

  ListStorage<FilteredRead>* result = createListStorage<FilteredRead>(use_disk);

  ThreadObject<int> counterSeq;
  ThreadObject<int> counterFLT3;
  ThreadObject<int> counterFLT3Rev;

  ISynchronizer* synchro = System::thread().newSynchronizer();
  

  Dispatcher dispatcher (nbThreads);
  // We loop over sequences.
  IDispatcher::Status status = dispatcher.iterate (ProgressIterator<Sequence>(*sequences), [&] (Sequence& seq) {
      if (seq.getDataSize() >= (size_t)m_kmerSize) {
        // ListStorage<std::shared_ptr<FilteredRead> > *localResult = new MemoryListStorage<std::shared_ptr<FilteredRead> >();
        int & localCounterSeq = counterSeq();
        int & localCounterFLT3 = counterFLT3();
        int & localCounterFLT3Rev = counterFLT3Rev();
        // We declare an iterator on a given sequence.
        Kmer<span>::ModelCanonical::Iterator itKmer (model);


        size_t nbKmers     = 0;
        size_t nbPresence = nbMatchedKmers(seq, itKmer, m_bloom, nbKmers);

        // si le kmer correspond à un FLT3
        if((nbKmers > 0 && (double)nbPresence/nbKmers > p) || ! use_bloom) {
          bool use_bloom_temp = use_bloom;
          nbKmers = 0;
          size_t nbPresenceRev = nbMatchedKmers(seq, itKmer, m_bloom_rev, nbKmers);
          bool is_rev = nbPresenceRev > (nbPresence - nbPresenceRev);
          if (is_rev && (double)(nbPresence - nbPresenceRev) / nbKmers > p) {
            // The issue is that with many false positives we could have both
            // sides that would be matching. In that case we must ignore the
            // result from the Bloom filter.
            is_rev = false;
            use_bloom_temp = false;
          }
          string sequence = (is_rev) ? seq.getRevcomp() : seq.toString();
          std::shared_ptr<FilteredRead> filtered(new FilteredRead(sequence, is_rev));
          itKmer.setData (seq.getData());
          size_t pos = 0;
          size_t matched_pos = 0;
          size_t vector_length = seq.getDataSize() - m_kmerSize + 1;
          for (itKmer.first(); !itKmer.isDone(); itKmer.next()) {
            string kmer;
            if (! is_rev)
              kmer = model.toString (itKmer->forward());
            else
              kmer = model.toString (itKmer->revcomp());
            if (m_hashMapTranscript.count(kmer) > 0) {
              filtered->addMatchingKmers((is_rev) ? vector_length - pos - 1 : pos,
                                         m_hashMapTranscript[kmer]);
              matched_pos++;
            } else if (! use_bloom || ! use_bloom_temp) {
              kmer = model.toString(itKmer->revcomp());
              if (m_hashMapTranscript.count(kmer) > 0) {
                filtered->addMatchingKmers( vector_length - pos - 1,
                                            m_hashMapTranscript[kmer]);
                is_rev = true;
                matched_pos++;
              }
            }
            pos++;
          }
          size_t nb_repeated = filtered->getNbRepeatedKmers(3);
          filtered->clean();
          if ((double)(matched_pos - nb_repeated) / vector_length > p) {
            localCounterFLT3++;
            if (is_rev)
              localCounterFLT3Rev++;
            if (outname.size() > 0)
              out->insert(seq);
            else {
              LocalSynchronizer sync (synchro);        
              result->appendData(filtered);
            }
          }
        }
        localCounterSeq++;
      }
    });

  counterSeq.foreach([&] (int n) { *counterSeq += n; });
  counterFLT3.foreach([&] (int n) { *counterFLT3 += n; });
  counterFLT3Rev.foreach([&] (int n) { *counterFLT3Rev += n; });

  nbSequences = *counterSeq;
  nbSequencesFLT3 = *counterFLT3;
  nbSequencesFLT3Rev = *counterFLT3Rev;
  
  if (outname.size() > 0)
    out->flush();

  delete synchro;
  
  return result;
}

std::vector<int> ClassFilter::getPositions(string kmer) {
  if (m_hashMapTranscript.count(kmer))
    return m_hashMapTranscript[kmer];
  return std::vector<int>();
}

std::string ClassFilter::getReference() const{
  return m_ref;
}

size_t ClassFilter::getReferenceLength() const{
  return m_ref_length;
}

ReferencePos ClassFilter::getReferencePos() {
  return m_ref_pos;
}

int ClassFilter::getKmerSize() const {
  return m_kmerSize;
}

void ClassFilter::setHighestFPRate(double highest) {
  highest_fp_rate = highest;
}
// void ClassFilter::displayResult(){
//   std::cout << "listes en retour après le filtre : " << '\n';
//   list< list< list<int> > >::iterator itr;
//   for (itr=result.begin(); itr != result.end(); itr++)
//   {
//     list<list<int>>sequence=*itr;
//     list< list<int> >::iterator it;
//     for (it=sequence.begin(); it != sequence.end(); it++)
//     {
//        list<int> positions = *it;
//        int nb = positions.size();
//        list<int>::iterator i;
//        for (i=positions.begin(); i != positions.end(); i++) {
//          int position = *i;
//          if(position == -1) {
//            std::cout << "N ";
//          }
//          else{
//            std::cout << position;
//            if(nb > 1) {
//              std::cout << ";";
//              nb--;
//            }
//          }
//        }
//        std::cout << " ";
//     }
//     std::cout << "\n\n" << '\n';
//   }
// }
