#include "Breakpoint.hpp"
#include "ConsensusSequence.hpp"

#include <cmath>
#include <cstdio>
#include <Class/Cleaning/CleanedRead.hpp>
#include <vector>

// Class Breakpoint
Breakpoint::Breakpoint():m_stop_pos(0),m_start_pos(0),m_gap(0),m_occurrence(0),m_total_occurrence(0),m_wt_occurrence(0),m_read_sum_length(0),m_k_confirm(0),m_abs_pos(make_tuple("",0,0)),m_ref_index(~0){}
Breakpoint::Breakpoint(size_t start_pos, size_t stop_pos, size_t gap):m_stop_pos(stop_pos),m_start_pos(start_pos),m_gap(gap),m_occurrence(0),m_total_occurrence(0),m_wt_occurrence(0),m_read_sum_length(0),m_k_confirm(0),m_abs_pos(make_tuple("",0,0)),m_ref_index(~0) {}


size_t Breakpoint::getStartPos() const{
  return m_start_pos;
}

size_t Breakpoint::getStopPos() const{
  return m_stop_pos;
}

size_t Breakpoint::getGapSize() const{
  return m_gap;
}

int Breakpoint::getFirstPosition() const {
  return m_first_pos;
}

size_t Breakpoint::getK() const {
  return m_k;
}

float Breakpoint::getMutatedOverWT() const{
  if (m_wt_occurrence == 0)
    return nan("");
  return correctedCount()*1./m_wt_occurrence;
}

std::string Breakpoint::getFullSequence() const {
  return full_sequence;
}

std::string Breakpoint::getSequence() const {
  return sequence;
}

size_t Breakpoint::getTotalOccurrence() const{
  return m_total_occurrence;
}

float Breakpoint::getVAF() const{
  if (m_total_occurrence == 0)
    return nan("");
  return correctedCount()*1. / m_total_occurrence;
}

size_t Breakpoint::getWTOccurrence() const{
  return m_wt_occurrence;
}

int Breakpoint::duplicationSize() const{
  return getStopPos() - getStartPos() + getGapSize() + 1;
}

void Breakpoint::addOccurrence(int pos_in_read, CleanedRead &read) {
  if (m_occurrence == 0) {
    m_k = read.getKmerSize();
    m_first_pos = pos_in_read;
  }
  supporting_reads.push_back(std::pair<size_t, CleanedRead&>(pos_in_read,read));
  m_occurrence++;
  m_read_sum_length += read.getCleanedPositions().size();
}

void Breakpoint::computeSequences(CoverageCounter &counter) {
  if (duplicationSize() > 0) {
    ConsensusSequence seq(*this);
    sequence = seq.getSequence();
    full_sequence = sequence;
    int shift = getStartPos();
    
    for (size_t i = 0; i < full_sequence.size(); i++) {
      if (full_sequence[i] == 'N') {
        full_sequence[i] = counter.get(i + shift, getVAF());
      }
    }
  }
}

size_t Breakpoint::count() const{
  return m_occurrence;
}

size_t Breakpoint::correctedCount() const{
  float avg_read_length = m_read_sum_length*1./m_occurrence;
  return count() * (1 + ((2*m_k_confirm - 1 + getGapSize() + getK() - 1) * 2 / avg_read_length));
}

std::string Breakpoint::exportJSON(bool debug) {
  std::string res = "{";
  res += "\"start_pos\" : " + std::to_string(getStartPos()) + ",";
  res += "\"stop_pos\" : " + std::to_string(getStopPos()) + ",";
  res += "\"size\" : " + std::to_string(duplicationSize()) + ",";
  res += "\"occurrence\" : " + std::to_string(correctedCount()) + ",";
  res += "\"raw_occurrence\" : " + std::to_string(count()) + ",";
  res += "\"region_coverage\" : "+ std::to_string(getTotalOccurrence()) +",";
  res += "\"wt_coverage\" : "+std::to_string(getWTOccurrence()) +",";
  res += "\"MT/WT\" : "+ to_string_nan(getMutatedOverWT()) +",";
  res += "\"VAF\": "+std::to_string(getVAF())+",";
  res += "\"break_size\" : "+std::to_string(getGapSize())+",";
  res += "\"is_duplication\" : " + bool_to_string(isDuplication())+",";
  res += "\"is_wt_duplication\" : "+ bool_to_string(isWTDuplication());

  if (duplicationSize() > 0) {
    res += ", \"sequence\": \""+ getSequence() +"\"";
    res += ", \"approximate_full_sequence\": \"" + getFullSequence()+"\"";
  }
  if (std::get<2>(m_abs_pos) != 0 && duplicationSize() > 0)
    res += ", \"reference_pos\": \"" + std::get<0>(m_abs_pos) + ":"
      + std::to_string(std::get<1>(m_abs_pos))
      + ":" + std::to_string(std::get<2>(m_abs_pos))+"\"";

  if (debug) {
    res += ", \"reads\": [";
    bool first = true;
    iterate();
    while (hasRead()) {
      if (! first)
        res += ",";
      first = false;
      res += getCurrentRead().exportJSON(debug);
      next();
    }
    res += "]";
  }
  return res+"}";
}

std::string Breakpoint::exportVCF(std::vector<std::string> &references, bool debug) {
  std::string res = "";

  if (std::get<2>(m_abs_pos) != 0 && duplicationSize() > 0 && m_ref_index != (size_t)~0) {
    int strand = std::get<2>(m_abs_pos);
    char nt = (strand==1) ? references[m_ref_index][getStartPos()-1] : revcomp(std::string(1,references[m_ref_index][getStopPos()+getGapSize()+1]), true)[0];
    
    res += std::get<0>(m_abs_pos) + "\t"
      + std::to_string(std::get<1>(m_abs_pos)-1)
      + "\t.\t"
      + nt+"\t"
      + nt+revcomp(getFullSequence(), strand == -1)+"\t.\t"
      + "length=" + std::to_string(duplicationSize())+";"
      + "M=" + std::to_string(correctedCount())+";"
      + "WT=" + std::to_string(getWTOccurrence())+";"
      + "M/WT=" + std::to_string(getMutatedOverWT())+";"
      + "VAF=" + std::to_string(getVAF())+";";
  }
  return res;
}

bool Breakpoint::isDuplication() const {
  // We have a duplication as soon as we come back in the reference
  // Without a duplication, with a gap we should have (k=3):
  // 5  6  7  _   _  10  11
  // If we have a duplication, 10 and 11 will be lower.
  // When having an insertion the gap is greater, so we must
  // only take into account the gap size with a repeat (getGapSize() < k)
  
  return getStopPos()+min(getGapSize(), getK()-1) >= getStartPos();
}

bool Breakpoint::isWTDuplication() const {
  return isDuplication()
    && m_ref.find(getFullSequence()) != std::string::npos;
}

bool Breakpoint::hasRead() const {
  return pos_iteration < supporting_reads.size();
}

void Breakpoint::iterate() {
  pos_iteration = 0;
}
void Breakpoint::next() {
  pos_iteration++;
}

int Breakpoint::getCurrentPos() const{
  return supporting_reads[pos_iteration].first;
}

CleanedRead &Breakpoint::getCurrentRead(){
  return supporting_reads[pos_iteration].second;
}

void Breakpoint::setReference(std::string ref) {
  m_ref = ref;
}

void Breakpoint::setReferencePos(ReferencePos refPos) {
  size_t ref_index = refPos.getReferenceIndex(getStopPos());

  if (ref_index == refPos.nb())
    return;

  m_ref_index = ref_index;

  bool strand = refPos.getStrand(ref_index);

  size_t rel_pos = getStartPos();
  if (! strand)
    // rev strand
    rel_pos = getStopPos()+getGapSize();

  m_abs_pos = refPos.getAbsolutePos(rel_pos);
}

void Breakpoint::setTotalOccurrence(size_t occ) {
  m_total_occurrence = occ;
}
void Breakpoint::setWTOccurrence(size_t occ) {
  m_wt_occurrence = occ;
}

void Breakpoint::setKConfirm(size_t k) {
  m_k_confirm = k;
}


// DiskBreakpoint

DiskBreakpoint::DiskBreakpoint(size_t start_pos, size_t stop_pos, size_t gap):Breakpoint(start_pos, stop_pos, gap),read_storage(std::tmpnam(nullptr)) {
  output = std::ofstream(read_storage, std::ios::binary);
}

DiskBreakpoint::DiskBreakpoint():DiskBreakpoint(0, 0, 0){}

DiskBreakpoint::~DiskBreakpoint() {
  remove(read_storage.c_str());
}

bool DiskBreakpoint::hasRead() const {
  return nb_reads_left >= 0;
}

void DiskBreakpoint::iterate() {
  output.close();
  nb_reads_left = m_occurrence;
  input = std::ifstream(read_storage, std::ios::binary);
  in_read = new cereal::BinaryInputArchive(input);
  next();
}

void DiskBreakpoint::next() {
  if (nb_reads_left > 0) {
    (*in_read)(stored_pos_in_read, stored_read);
  }
  nb_reads_left--;
  if (nb_reads_left == 0)
    delete in_read;
}

int DiskBreakpoint::getCurrentPos() const {
  return stored_pos_in_read;
}

CleanedRead &DiskBreakpoint::getCurrentRead() {
  return stored_read;
}

Breakpoint *createBreakpoint(size_t start_pos, size_t stop_pos, size_t gap, bool on_disk) {
  if (on_disk)
    return new DiskBreakpoint(start_pos, stop_pos, gap);
  return new Breakpoint(start_pos, stop_pos, gap);
}
