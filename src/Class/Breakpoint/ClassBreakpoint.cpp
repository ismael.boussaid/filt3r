#include "ClassBreakpoint.hpp"
#include "BreakpointAnalyser.hpp"
#include <Tools/filter_tools.hpp>
#include <Tools/CoverageCounter.hpp>
#include <Tools/ReferencePos.hpp>

/**
Classe permettant de réaliser l'étape 3 c'est à dire de trouver
les points d'arrets.
*/
ClassBreakpoint::ClassBreakpoint(ListStorage < CleanedRead > *result, std::string ref, bool on_disk):m_sequences(result),m_ref(ref),on_disk(on_disk)
{}

ClassBreakpoint::~ClassBreakpoint() {
  for (auto entry=m_result.begin() ; entry != m_result.end(); entry++) {
    delete entry->second;
  }
}

void ClassBreakpoint::searchBreakpoints(int nb_kmers_confirming,
                                        int min_nb_occ,
                                        ReferencePos refPos,
                                        int ignore_short_indels) {
  CoverageCounter total_coverage(m_ref.length());
  m_sequences->iterate();
  while(m_sequences->hasData()) {
    std::shared_ptr<CleanedRead> read = m_sequences->getCurrentData();
    BreakpointAnalyser bpa(read, on_disk);
    total_coverage.add(*read);
    std::list<Breakpoint*> breakpoints = bpa.findBreakpoints(nb_kmers_confirming,
                                                            ignore_short_indels);
    for (auto breakpoint: breakpoints) {
      breakpoint->setKConfirm(nb_kmers_confirming);
      std::string id = std::to_string(breakpoint->getStopPos())+"-"
        + std::to_string(breakpoint->getStartPos())+"-"
        + std::to_string(breakpoint->duplicationSize());

      if (m_result.count(id) == 0) {
        m_result.insert(std::pair<string, Breakpoint*>(id, breakpoint));
	breakpoint->setReference(m_ref);
      } else {
        m_result[id]->addOccurrence(breakpoint->getFirstPosition(), *read);
        delete breakpoint;
      }
    }

    m_sequences->next();
  }
  CoverageCounter wt_coverage(total_coverage);
  for (auto entry=m_result.begin() ; entry != m_result.end(); ) {
    // remove coverage from the WT coverage
    entry->second->iterate();
    while (entry->second->hasRead()) {
      wt_coverage.add(entry->second->getCurrentRead(), -1);
      entry->second->next();
    }
    if ((int) entry->second->count() < min_nb_occ) {
      delete entry->second;
      entry = m_result.erase(entry);
    } else
      entry++;
  }
  for (auto entry=m_result.begin() ; entry != m_result.end(); entry++) {
    entry->second->setReferencePos(refPos);
    size_t correction = entry->second->correctedCount() - entry->second->count();
    size_t wt = (wt_coverage[entry->second->getStartPos()] + wt_coverage[entry->second->getStopPos()]) / 2;
    wt = (wt > correction) ? wt - correction : 0;
    size_t total = max(wt+entry->second->correctedCount(), total_coverage.getAverage(entry->second->getStartPos()-1, entry->second->getStopPos()+entry->second->getK()));
    // Correct for the reads in which we couldn't detect a breakpoint. They must be removed from WT.
    entry->second->setTotalOccurrence(total);
    entry->second->setWTOccurrence(wt);
    entry->second->computeSequences(total_coverage);
  }
}

size_t ClassBreakpoint::count() {
  return m_result.size();
}

std::map<std::string, Breakpoint*> &ClassBreakpoint::getResults() {
  return m_result;
}
