#include "BreakpointAnalyser.hpp"
#include <Tools/filter_tools.hpp>

BreakpointAnalyser::BreakpointAnalyser(std::shared_ptr<CleanedRead> read, bool on_disk):read(read),on_disk(on_disk){}

std::list<Breakpoint*> BreakpointAnalyser::findBreakpoints(int nb_kmers_confirming,
                                                          int ignore_short_indels) {
  std::list<std::pair<size_t, size_t> > start_pos = findStartStop(nb_kmers_confirming);
  list<Breakpoint*> breakpoints;
  vector<int> &cleaned_pos = read->getCleanedPositions();
  for (std::pair<size_t, size_t> pos: start_pos) {
    // Iterate on each stop/start pos to check whether it is convincing

    size_t stop_pos = pos.first, start_pos = pos.second;
    bool consistent = true;

    size_t stop = stop_pos - nb_kmers_confirming + 1;
    
    for (int i = 1; i < nb_kmers_confirming && consistent; i++) {
      consistent = (cleaned_pos[stop] + i == cleaned_pos[stop+i])
        && (cleaned_pos[start_pos] + i == cleaned_pos[start_pos + i]);
    }

    if (consistent) {
      Breakpoint *bp = createBreakpoint(cleaned_pos[start_pos], cleaned_pos[stop_pos], start_pos - stop_pos - 1, on_disk);
      if (! ignore_short_indels || abs(bp->duplicationSize()) > ignore_short_indels) {
        bp->addOccurrence(start_pos, *read);
        breakpoints.push_back(bp);
      } else {
        delete bp;
      }
    }
  }
  return breakpoints;
}

std::list<std::pair<size_t, size_t> > BreakpointAnalyser::findStartStop(int nb_kmers_confirming) {
  bool is_in_N = true;
  int length_before = 0, length_after = 0;
  size_t candidate_stop_pos=0,
    candidate_start_pos = 0, pos=0;
  std::list<std::pair<size_t, size_t> > results;
  for(int i: read->getCleanedPositions()) {
    if (i == -1) {
      if (! is_in_N) {
        if (candidate_stop_pos > 0 && length_before >= nb_kmers_confirming
            && length_after >= nb_kmers_confirming)  {
          results.push_back(make_pair(candidate_stop_pos, candidate_start_pos));
        }
        candidate_stop_pos = pos-1;
      }
      is_in_N = true;
    } else {
      if (is_in_N) {
        candidate_start_pos = pos;
        length_before = length_after;
        length_after = 0;
      }
      length_after++;
      is_in_N = false;
    }
    pos++;
  }
  if (! is_in_N) {
    if (candidate_stop_pos > 0 && length_before >= nb_kmers_confirming
        && length_after >= nb_kmers_confirming)  {
      results.push_back(make_pair(candidate_stop_pos, candidate_start_pos));
    }
  }
  return results;
}
