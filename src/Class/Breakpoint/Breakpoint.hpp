#ifndef BREAKPOINT_HPP
#define BREAKPOINT_HPP

#include <vector>
#include <string>
#include <utility>
#include <fstream>
#include <Class/Cleaning/CleanedRead.hpp>
#include <Tools/CoverageCounter.hpp>
#include <Tools/ReferencePos.hpp>
#include <cereal/archives/binary.hpp>

class Breakpoint
{
  public:
  using iterator = vector<pair<int, CleanedRead &> >::iterator;
  using const_iterator = vector<pair<int, CleanedRead &> >::const_iterator;
  
  Breakpoint();
  /**
   * Represents a breakpoint having the following type
   * 285 286 287 288 289 290 N N N N N 239 240
   * In such a case, start_pos=239, stop_pos=290, gap=5
   *
   * Otherwise stated stop_pos is the position when the sequence stops before
   * the breakpoint, start_pos is the position when the sequence starts after
   * the breakpoint
   * @post count() == 0
   */
  Breakpoint(size_t start_pos, size_t stop_pos, size_t gap);

  virtual ~Breakpoint() {};
  
  size_t getStartPos() const;
  size_t getStopPos() const;
  size_t getGapSize() const;

  /**
   * Iterator on reads
   */ 
  inline iterator begin() noexcept {return supporting_reads.begin();}
  /**
   * Iterator on reads
   */ 
  inline iterator end() noexcept {return supporting_reads.end();}
  inline const_iterator cbegin() const noexcept { return supporting_reads.cbegin(); }
  inline const_iterator cend() const noexcept { return supporting_reads.cend(); }
   
  /**
   * Get the position at which the breakpoint occurs (`stop_pos`) in the first
   * read of that breakpoint
   */
  int getFirstPosition() const;
  
  /**
   * @return the k-mer used
   */
  size_t getK() const;
  
  /**
   * @return the MT/WT ratio
   */
  float getMutatedOverWT() const;

  /**
   * @pre duplicationSize() > 0
   * @return a consensus sequence of the duplication. That sequence may contain N
   * when the duplication spans several reads. The sequence will be expanded as
   * much as possible but this sequence is less reliable as the one coming from
   * getSequence() as we try to fill the missing nucleotides.
   */
  std::string getFullSequence() const;
  
  /**
   * @pre duplicationSize() > 0
   * @return a consensus sequence of the duplication. That sequence may contain N
   * when the duplication spans several reads.
   */
  std::string getSequence() const;
  
  size_t getTotalOccurrence() const;
  
  /**
   * @return VAF
   */
  float getVAF() const;

  size_t getWTOccurrence() const;

  bool isDuplication() const;

  /**
   * @return true iff isDuplication() and if the duplication
   * comes from the wildtype sequence.
   */
  bool isWTDuplication() const;
  
  /**
   * Returns the size of the duplication (assuming it is a duplication).  
   * The returned value may be negative when the breakpoint doesn't 
   * correspond to a duplication.
   * @return getStopPos() - getStartPos() + getGapSize() + 1
   */
  int duplicationSize() const;
  
  /**
   * Number of times the breakpoint was seen
   */
  size_t count() const;

  /**
   * Compute the sequences corresponding to the duplication.
   *
   * Performs something only when duplicationSize() > 0
   * @complexity O(n×l), where n is the number of reads supporting the duplication and l
   * is duplicationSize().
   */
  void computeSequences(CoverageCounter &counter);
  
  /**
   * Number of times the breakpoint should have been seen.
   * The breakpoints that are at one extremity of a read cannot be processed.
   * Thus we correct the counts for this issue.
   */
  size_t correctedCount() const;

  /**
   * The method does not check that the read actually contains such an
   * occurrence.
   * `pos_in_read` is the starting position of the duplication (`start_pos`) 
   * in the cleaned read.
   * More specifically it is the position of getStartPos() in the read.
   * @post old count() +1 == count()
   */
  virtual void addOccurrence(int pos_in_read, CleanedRead &read);

  std::string exportJSON(bool debug=false);
  std::string exportVCF(std::vector<std::string> &references, bool debug=false);

  /**
   * Sets the reference sequence used
   */
  void setReference(std::string ref);

  void setReferencePos(ReferencePos refPos);
  
  /**
   * Sets the total occurrence at the loci
   */
  void setTotalOccurrence(size_t occ);
  /**
   * Sets the occurrence for the WT
   */
  void setWTOccurrence(size_t occ);

  /**
   * Sets the number of k-mers used to confirm a breakpoint
   */
  void setKConfirm(size_t k);

  // ITERATION

  /**
   * True if a read exists at the current position
   */
  virtual bool hasRead() const;
  
  /**
   * Initialize the iteration over the reads stored.
   */
  virtual void iterate();

  virtual void next();

  /**
   * @pre hasRead()
   */
  virtual int getCurrentPos() const;
  /**
   * @pre hasRead()
   */ 
  virtual CleanedRead &getCurrentRead();
protected:
  size_t m_stop_pos;
  size_t m_start_pos;
  size_t m_gap;
  size_t m_occurrence;
  size_t m_total_occurrence;
  size_t m_wt_occurrence;
  size_t m_k;
  int m_first_pos;
  u_int64_t m_read_sum_length;
  size_t m_k_confirm;
  size_t pos_iteration;
  std::string sequence, full_sequence, m_ref;
  std::tuple<string,size_t,int> m_abs_pos; // reference name, reference pos, strand (-1 or 1)
  size_t m_ref_index;			   // index of the reference sequence
private:
  vector<pair<int, CleanedRead &> > supporting_reads;
};


class DiskBreakpoint: public Breakpoint {
public:
  DiskBreakpoint();
  DiskBreakpoint(size_t start_pos, size_t stop_pos, size_t gap);
  ~DiskBreakpoint() override;
  
  /**
   * @inherited
   */
  void addOccurrence(int pos_in_read, CleanedRead &read) override {
    if (m_occurrence == 0) {
      m_k = read.getKmerSize();
      m_first_pos = pos_in_read;
    }
    cereal::BinaryOutputArchive out_read(output);
    out_read(pos_in_read, read);
    m_occurrence++;
    m_read_sum_length += read.getCleanedPositions().size();
  }

  /** @inherited */
  bool hasRead() const override;

  /** @inherited */
  void iterate() override;

  /** @inherited */
  void next() override;

  /** @inherited */
  int getCurrentPos() const override;

  /** @inherited */
  CleanedRead &getCurrentRead() override;
  
private:
  
  std::string read_storage;
  std::ifstream input;
  std::ofstream output;
  cereal::BinaryInputArchive *in_read;
  CleanedRead stored_read;
  int stored_pos_in_read;
  int nb_reads_left;
};


/**
 * Create a Breakpoint with the three first parameters.
 * The last parameter determines whether a DiskBreakpoint or a Breakpoint is created.
 */
Breakpoint *createBreakpoint(size_t start_pos, size_t stop_pos, size_t gap, bool on_disk);
#endif
