#ifndef CONSENSUS_SEQUENCE_HPP
#define CONSENSUS_SEQUENCE_HPP

#include "Breakpoint.hpp"

#include <string>
#include <vector>

class ConsensusSequence {

private:
  Breakpoint &breakpoint;
  std::string consensus;
  std::vector<size_t> coverage;

  void computeConsensus();
public:
  /**
   * Builds a consensus sequence for the given breakpoint
   *
   * @pre breakpoint's duplicationSize() > 0
   */
  ConsensusSequence(Breakpoint &);

  std::string getSequence() const;

  std::vector<size_t> getCoverage() const;

  
};

#endif
