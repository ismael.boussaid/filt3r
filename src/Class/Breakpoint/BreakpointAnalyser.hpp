#ifndef BREAKPOINT_ANALYSER_HPP
#define BREAKPOINT_ANALYSER_HPP
#include <utility>
#include "Breakpoint.hpp"
#include <Class/Cleaning/CleanedRead.hpp>
#include <memory>

class BreakpointAnalyser {
public:
  BreakpointAnalyser(std::shared_ptr<CleanedRead> , bool on_disk = false);

  /**
   * Return a list of the breakpoints found.
   * `nb_kmers_confirming` is the number of kmers that must be used
   * from each part of the breakpoint to confirm it.
   * We check that the positions are consistent between those k-mers.
   * Thus to do so we need at least 2 kmers.
   * `ignore_short_indels` is the length of the indels to ignore
   * (set to 0 to take into account all short indels)
   */
  std::list<Breakpoint*> findBreakpoints(int nb_kmers_confirming,
                                        int ignore_short_indels=0);

  /**
   * Find the start position of all the breakpoints (either for duplications
   * or not)
   */
  std::list<std::pair<size_t, size_t> > findStartStop(int nb_kmers_confirming);

private:
  std::shared_ptr<CleanedRead> read;
  bool on_disk;
};

#endif
