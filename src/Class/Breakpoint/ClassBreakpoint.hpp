#include <string>
#include <vector>
#include <map>
#include <iostream>
#include "Breakpoint.hpp"
#include <Class/Cleaning/CleanedRead.hpp>
#include <Tools/ListStorage.hpp>

class ClassBreakpoint
{
    public:
  ClassBreakpoint(ListStorage<CleanedRead> *result,
                  std::string ref,
                  bool on_disk = false);

  ~ClassBreakpoint();

    /**
     * @param nb_kmers_confirming: minimal number of kmers
     *        on each side of the breakpoint to confirm
     * @param min_nb_occ: minimal number of occurrences 
     *                    to keep the breakpoint
     * @param ignore_short_indels: length of the indels to ignore (must be >=0),
     *                             if set to 0 all indels are taken into account
     */
    void searchBreakpoints(int nb_kmers_confirming,
                           int min_nb_occ,
                           ReferencePos refPos,
                           int ignore_short_indels=0);
    // void Breakpoint(double percentageFiabilitySequence);
    // void BreakpointSequence(CleanedRead &sequence, double percentageFiabilitySequence);
    // double verificationSequence(std::vector<int> &sequence);
    // void analyse(int elementMemoirePrecedent, int elementMemoireSuivant, int nbInGroupOfN, std::vector<int> &sequence, double percentageFiabilitySequence);
    // void displayResult();
    // std::map<std::string, Breakpoint*> getMap();

    /**
     * @return the number of breakpoints
     */
    size_t count();

    std::map<std::string, Breakpoint*> &getResults();


    private:
    ListStorage < CleanedRead > *m_sequences;
    std::string  m_ref;
    std::map<std::string, Breakpoint*> m_result;
    bool on_disk;
};
