#include "ConsensusSequence.hpp"
#include <algorithm>

ConsensusSequence::ConsensusSequence(Breakpoint &bp):breakpoint(bp), coverage(bp.duplicationSize()){
  computeConsensus();
}

void ConsensusSequence::computeConsensus() {
  int n = breakpoint.duplicationSize();
  std::vector<std::vector<size_t> > counts(n);

  for (size_t i = 0; i < counts.size(); i++) {
    counts[i] = std::vector<size_t>(4);
  }

  breakpoint.iterate();
  while (breakpoint.hasRead()) {
    CleanedRead &read = breakpoint.getCurrentRead();
    std::string seq = read.getFilteredRead()->getSequence();

    // getCurrentPos() gives the starting position of the second version
    // of the duplication.
    // However we prefer taking the sequence from the firt version for compatibility with other software.
    // So we will get the sequence by its end.
    int end = breakpoint.getCurrentPos() - 1;

    // All the reads are already on the forward strand.
    
    // In the forward sense we will get the sequence that appears before
    // the breakpoint, that is the end of the sequence.

    // If we reverse the reference, we may not have the same consensus sequence,
    // especially with repeats at one ends.
    // In the functional tests, this is specified with the several posibilities we may have

    // count each nucleotide
    for (int j = 0; j < n && end - j >= 0; j++) {
      counts[n-1-j][nuc_to_int(seq[end-j])]++;
      coverage[n-1-j]++;
    }

    breakpoint.next();
  }

  consensus = std::string(n, 'N');
  char dna[] = {'A', 'C', 'G', 'T'};
  for (int i = 0; i < n; i++) {
    if (coverage[i] > 0) {      // TODO more stringent criteria
      auto max = std::max_element(counts[i].begin(), counts[i].end());
      auto max_i = max - counts[i].begin();
      consensus[i] = dna[max_i];
    }
  }
}

std::string ConsensusSequence::getSequence() const {
  return consensus;
}

std::vector<size_t> ConsensusSequence::getCoverage() const {
  return coverage;
}


