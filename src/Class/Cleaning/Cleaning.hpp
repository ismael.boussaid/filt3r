#ifndef CLEANING_HPP
#define CLEANING_HPP
#include <list>
#include <ostream>
#include <Class/Filter/FilteredReads.hpp>
#include <Class/Filter/ClassFilter.hpp>
#include <Tools/ListStorage.hpp>
#include "CleanedRead.hpp"

class Cleaning {
public:
  /**
   * @param kmer_extension: number of kmers under which we can merge breaks
   */
  Cleaning(ListStorage<FilteredRead> *filtered,
           ClassFilter *filter, int kmer_extension, bool remove_mutations = true,
           bool remove_gaps = true, bool use_disk = false);

  ~Cleaning();
  
  ListStorage<FilteredRead> *filtered;
  ListStorage<CleanedRead> *cleaned;
  ClassFilter *filter;
};

std::ostream &operator<<(std::ostream &, Cleaning &);
#endif
