#include "CleanedRead.hpp"
#include <limits>
#include <utility>
#include <Tools/KmerPosAnalyser.hpp>
#include <Tools/filter_tools.hpp>
#include <Class/Filter/ClassFilter.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/memory.hpp>

CleanedRead::CleanedRead():filtered(NULL),corrected_read(),cleaned_positions(0),filter(nullptr){}
CleanedRead::CleanedRead(std::shared_ptr<FilteredRead> filtered, ClassFilter *filter, int kmer_extension):
  filtered(filtered),corrected_read(),cleaned_positions(filtered->size()), filter(filter), kmer_extension(kmer_extension),kmer_size(filter->getKmerSize())
{}

CleanedRead::CleanedRead(CleanedRead &cleaned):filtered(cleaned.filtered), cleaned_positions(cleaned.cleaned_positions),filter(cleaned.filter),kmer_extension(cleaned.kmer_extension),kmer_size(cleaned.kmer_size){}

void CleanedRead::clean(bool remove_mutations, bool remove_gaps)
{
  removeAmbiguities();
  if (remove_gaps)
    removeSpuriousGaps();
  if (remove_mutations)
    removeMutations();
}

int CleanedRead::getKmerSize() const {
  return kmer_size;
}

void CleanedRead::removeAmbiguities() {
  size_t len = filtered->size();
  KmerPosAnalyser kpa(filtered->getPositions());
  
  std::vector<std::pair<size_t, size_t> > gaps = kpa.getGaps();
  int previous_gap = -1, next_gap = 0;

  size_t last_pos_set = ~0;
  
  for (size_t i = 0; i < len; i++) {
    if (next_gap < (int)gaps.size() && i >= gaps[next_gap].first) {
      previous_gap = next_gap;
      next_gap++;
    }
    std::vector<int> &current_pos = filtered->getMatchingPositions(i);

    if (current_pos.size() == 0)
      cleaned_positions[i] = -1;
    else if (current_pos.size() == 1)
      cleaned_positions[i] = current_pos[0];
    else {

      // First we will check whether the previously chosen position is a good
      // candidate
      bool consistent_suite = false;
      if (i > 0 && cleaned_positions[i-1] > -1) {
        auto found = std::find(current_pos.begin(), current_pos.end(), cleaned_positions[i-1]+1);
        if (found != current_pos.end()) {
          consistent_suite = true;
          cleaned_positions[i] = cleaned_positions[i-1]+1;
        }
      }

      // Otherwise we will try harder
      if (! consistent_suite) {
        float min_distance = std::numeric_limits<float>::max();
        int min_value;

        size_t previous_pos = 0;
        if (previous_gap > -1)
          // We go until the previous gap
          previous_pos = gaps[previous_gap].second+1;
        if (previous_gap > 0 && previous_pos==i)
          // If we are just after a gap, we allow to cross it
          previous_pos = gaps[previous_gap-1].second+1;
        size_t last_pos = len-1;
        if (next_gap < (int)gaps.size())
          // We go until the next gap
          last_pos = gaps[next_gap].first - 1;
        if (next_gap+1 < (int)gaps.size() && i == last_pos)
          // If we are just before a gap, we allow to cross it
          last_pos = gaps[next_gap+1].first - 1;

        if (previous_pos == last_pos) {
          // We have just one isolated position
          // This position may later be removed by
          // removeSpuriousGaps()

          previous_pos = std::max((size_t)30, i)-30; // arbitrarily set
          last_pos = min(len-1, i+30);
        } 
        for (int value: current_pos) {
          float distance = std::numeric_limits<float>::max();
          float distance_cp = std::numeric_limits<float>::max();
          
          if (previous_pos != i || last_pos != i)
            distance = kpa.getDistance(value, previous_pos, last_pos, i);
          if (last_pos_set < (size_t)~0 && last_pos_set >= previous_pos) {
            distance_cp = kpa.getNonAmbiguousDistance(cleaned_positions, value, previous_pos, i-1, i);
          }

          if (distance_cp <= min_distance) {
            min_value = value;
            min_distance = distance;
          }
          if (distance < min_distance) {
            min_value = value;
            min_distance = distance;
          }
        }

        if (min_distance < std::numeric_limits<float>::max())
          cleaned_positions[i] = min_value;
        else {
          // values have not been determined.
          // we should not reach this point
          assert(false);
        }
      }
    }
    if (current_pos.size() != 0) {
      last_pos_set = i;
    }
  }
}

void CleanedRead::removeMutations() { 
  size_t len = filtered->size();

  // Convert cleaned vector to a filtered-like vector
  std::vector<std::vector<int> > ppos(getCleanedPositions().size());
  for (size_t i = 0 ; i < ppos.size(); i++) {
    if (getCleanedPositions()[i] >= 0)
      ppos[i].push_back(getCleanedPositions()[i]);
  }
  KmerPosAnalyser kpa(ppos);
  
  std::vector<std::pair<size_t, size_t> > gaps = kpa.getGaps();
  int kmer_size = filter->getKmerSize();
  std::string read = filtered->getSequence();
  for (auto pair : gaps) {
    size_t start = pair.first;
    size_t stop = pair.second;
    if (start > 0 && stop < len - 1) {
      if (cleaned_positions[stop+1] - cleaned_positions[start-1]
          == (int) ((stop+1) - (start-1))) {
        // substitutions
        bool subst_compatible = true;
        size_t i = start;
        while (i <= stop && subst_compatible) {
          // Extract kmer
          std::string kmer = read.substr(i, kmer_size);
          // Try each correction
          for (std::string corrected_kmer: getModifiedSequences(kmer, kmer_size - 1)) {
            vector<int> match_pos = filter->getPositions(corrected_kmer);
            auto it = std::find(match_pos.begin(), match_pos.end(),
                                cleaned_positions[start-1] + (i - start) + 1);
            if (it != match_pos.end()) {
              cleaned_positions[i] = cleaned_positions[start - 1] + (i - start) + 1;
              read[i+kmer_size-1] = corrected_kmer[kmer_size-1];
              subst_compatible = true;
              break;
            }
            subst_compatible = false;
          }
          i++;
        }
      }
    }
  }
  corrected_read = read;
}

void CleanedRead::removeSpuriousGaps() {
  std::vector<std::pair<size_t, size_t> > gaps = get_cleaned_gaps(cleaned_positions);
  
  bool break_merged = false;
  for (size_t i = 0; i < gaps.size() - 1; i++) {
    if (! break_merged
        && gaps[i].first > 0 && gaps[i+1].second < cleaned_positions.size()-1
        && gaps[i+1].first - gaps[i].second -1 < (size_t)kmer_extension) {
      // The gaps are sufficiently close to be merged
      int length_cumul = cleaned_positions[gaps[i].first-1] - cleaned_positions[gaps[i+1].second+1];
      int length1 = cleaned_positions[gaps[i].first - 1] - cleaned_positions[gaps[i].second+1];
      int length2 = cleaned_positions[gaps[i+1].first - 1] - cleaned_positions[gaps[i+1].second+1];
      if ((length1 * length2 < 0 && length_cumul > 0)
          || length1 > length_cumul || length2 > length_cumul) {
        // Merge breaks
        for (size_t j = gaps[i].second + 1; j < gaps[i+1].first; j++) {
          cleaned_positions[j] = -1;
        }
        break_merged = true;
      }
    } else {
      break_merged = false;
    }
  }
}

CleanedRead& CleanedRead::operator=(CleanedRead&& read) {
  this->filtered = std::exchange(read.filtered, nullptr);
  this->filter = std::exchange(read.filter, nullptr);
  this->cleaned_positions = std::exchange(read.cleaned_positions, std::vector<int>(0));
  this->corrected_read = read.corrected_read;
  this->kmer_extension = read.kmer_extension;
  this->kmer_size = read.kmer_size;
  return *this;
}
CleanedRead &CleanedRead::operator=(const CleanedRead &c) {
  this->filtered = c.filtered;
  this->corrected_read = c.corrected_read;
  this->cleaned_positions = c.cleaned_positions;
  this->kmer_extension = c.kmer_extension;
  this->kmer_size = c.kmer_size;
  return *this;
}

std::shared_ptr<FilteredRead> CleanedRead::getFilteredRead() {
  return filtered;
}

std::vector<int> &CleanedRead::getCleanedPositions() {
  return cleaned_positions;
}

std::string &CleanedRead::getCorrectedRead() {
  return corrected_read;
}

std::string CleanedRead::exportJSON(bool debug) {
  std::string out = "{\"sequence\": \"" + filtered->getSequence()+"\",";
  out += "\"decompositions\": {\"cleaned\": " + vector_to_json(getCleanedPositions());

  out += ", \"original\": [";
  bool start = true;
  for (size_t i = 0; i < filtered->size(); i++) {
    if (! start)
      out += ", ";
    start = false;
    out += vector_to_json<int>(filtered->getMatchingPositions(i));
  }
  out += "]}}";
  return out;
}

std::ostream &operator<<(std::ostream &out, CleanedRead &r) {
  out << *r.getFilteredRead() << endl;
  for (int i: r.getCleanedPositions()) {
    if (i == -1)
      out << ".";
    else
      out << i;
    out << " ";
  }
  return out;
}
