#ifndef CLEANED_READ_HPP
#define CLEANED_READ_HPP

#include <string>
#include <list>
#include <iostream>
#include <memory>
#include <istream>
#include <ostream>

#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>

class CleanedRead;

#include <Class/Filter/ClassFilter.hpp>
#include <Class/Filter/FilteredReads.hpp>

/**
Classe permettant de réaliser l'étape 2 c'est à dire de nettoyer
l'ensemble de séquences afin d'y voir plus clair pour la recherche
des points d'arrets
*/
class CleanedRead
{
public:
  CleanedRead();
  
  CleanedRead(std::shared_ptr<FilteredRead> filtered, ClassFilter *filter,
              int kmer_extension);
  /**
   * Share the same references
   */
  CleanedRead(CleanedRead &cleaned);
  void displayResult();
  int findPosition(int elementMemoire, std::vector<int> &element);

  std::shared_ptr<FilteredRead> getFilteredRead();
  vector<int> &getCleanedPositions();

  std::string &getCorrectedRead();

  int getKmerSize() const;  

  CleanedRead& operator=(CleanedRead&&);

  void clean(bool remove_mutations = true, bool remove_gaps = true);

  /**
   * Remove multiple locations in the input vector
   */
  void removeAmbiguities();

  /**
   * @pre removeAmbiguities has already been launched.
   *      removeSpuriousGaps() should be launched before too.
   */
  void removeMutations();

  /**
   * @pre removeAmbiguities has already been launched
   */
  void removeSpuriousGaps();

  template <class Archive>
  void save(Archive &ar) const{
    ar(*filtered, corrected_read, cleaned_positions, kmer_extension, kmer_size);
  }    
  template <class Archive>
  void load(Archive &ar, ClassFilter *filter=NULL) {
    FilteredRead read;
    ar(read, corrected_read, cleaned_positions, kmer_extension, kmer_size);
    filtered = std::make_shared<FilteredRead>(read);
  }

  std::string exportJSON(bool debug=false);

  CleanedRead &operator=(const CleanedRead &);
private:

  std::shared_ptr<FilteredRead> filtered;
  std::string corrected_read;
  std::vector<int> cleaned_positions;
  ClassFilter *filter;
  int kmer_extension;
  int kmer_size;
};

std::ostream &operator<<(std::ostream &, CleanedRead &);
#endif
