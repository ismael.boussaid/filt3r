#include "Cleaning.hpp"
#include "CleanedRead.hpp"
#include <algorithm>

#include <Tools/filter_tools.hpp>

Cleaning::~Cleaning() {
  delete cleaned;
}

Cleaning::Cleaning(ListStorage<FilteredRead> *filtered, ClassFilter *filter, int kmer_extension, bool remove_mutations, bool remove_gaps, bool use_disk):filtered(filtered), filter(filter){
  
  //std::transform(filtered.begin(), filtered.end(), cleaned.begin(), buildCleanedRead);
  filtered->iterate();
  cleaned = createListStorage<CleanedRead>(use_disk);
  while (filtered->hasData()) {
    std::shared_ptr<CleanedRead> read = std::make_shared<CleanedRead>(filtered->getCurrentData(), filter, kmer_extension);
    read->clean(remove_mutations, remove_gaps);
    cleaned->appendData(read);
    filtered->next();
    read.reset();
  }
}

std::ostream &operator<<(std::ostream &out, Cleaning &c) {
  c.cleaned->iterate();
  while (c.cleaned->hasData()) {
    out << *(c.cleaned->getCurrentData()) << std::endl;
    c.cleaned->next();
  }
  return out;
}
