

# Filtering correct sequences

The reference sequence is indexed in two data structures: 

1. A bloom filter
2. A hash table, where $k$-mer positions are stored.


Then all reads are traversed. For each read we query the bloom filter to
determine how many $k$-mers match the reference region.  If the proportion of
matching $k$-mers is above a given threshold, the read is considered as
originating from the reference region. Such a read will be stored as a
`FilteredRead`.

All this word is achieved in the `ClassFilter` class.

# Cleaning reads

Once the reads have been processed, the `FilteredReads` are cleaned to prevent
false positives.

The cleaning (achieved in `CleanedRead`) consists of three steps:

1. Removing ambiguities (`CleanedRead::removeAmbiguities`)
2. Removing spurious breaks (`CleanedRead::removeSpuriousGaps`)
3. Removing substitutions (`CleanedRead::removeMutations`)

## Removing ambiguities

Whenever a *k*-mer occurs at several loci in the reference sequence, we
disambiguate this in the read using the context of all the *k*-mers.

## Removing spurious breaks

TBD

## Removing substitutions

Substitutions are the most frequent mutations and sequencing errors (at least
for Illumina sequencers). Thus many breaks will be due to substitutions.  To
limit the number of breaks to consider and to more valid positions, we remove
substitutions by correcting all substitutions (either mutations or sequencing
errors).

# Calling breakpoints

Once reads have been cleaned, we next have to find breakpoints in the cleaned
reads (which, themselves are only filtered reads).
The class `ClassBreakpoint` deals with that.

For each cleaned read, a `BreakpointAnalyser` will try to find a breakpoint
that could be due to a duplication (but which, more generally, could also be
due to an indel).

For a duplication to be found we need to have several consistent *k*-mers on
each side of the break.

## Coverage

The coverage is computed with a vector of integers that stores for each
position of the reference sequence, the number of reads that have identified a
*k*-mer at each of the positions.

This coverage is counted for each cleaned read, given their cleaned positions.
In such a case, the coverage of duplicated regions will be overestimated.
Thus, we take the coverage just before and just after the duplication to prevent this issue.
Other ideas were explored that could be restored one day if needed (see issue #18).

Then, once the breakpoints have been identified, the coverage for the
breakpoint reads are removed to infer the wild-type coverage.

The counts are corrected to take into account the fact that duplications on
the border of a read cannot be detected. Those corrections are removed from
the WT counts. The raw and corrected counts are both available in the output.
This correction is performed in `Breakpoint::correctedCount`.

The total coverage of the region (used to compute the VAF) is deduced either from the coverage obtained when taking the position before and the position after the duplication or from the sum of the MT (corrected count) + wild-type coverages, depending on which is higher.
