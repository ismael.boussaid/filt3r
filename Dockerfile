from gcc:11

run apt-get update && apt-get install -y  git cmake time
run git clone https://gitlab.univ-lille.fr/filt3r/filt3r.git
run cd filt3r && make gatb && make
